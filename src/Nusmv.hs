{-# LANGUAGE DataKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE Rank2Types #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE TemplateHaskell #-}

module Nusmv where

import Control.Lens
import Control.Parallel.Strategies
import Data.List
import Data.Semigroup
import Data.Tuple
import qualified Data.List.NonEmpty as Nel
import Data.Kind

data NType = NBool
           | NInt
           | NEnum
           | NWord
           | NArray NType
           | NSet NType
           | NModule

class NShow t where nshow :: t -> String

class Embeddable t u where embed :: t -> u
instance Embeddable t t where embed = id
--instance (Embeddable t u, Embeddable u v) => Embeddable t v where embed = embed . embed

class Prop t where
  (/\) :: t -> t -> t
  (\/) :: t -> t -> t
  neg :: t -> t
  falsum :: t
  verum :: t
  _AND :: Foldable f => f t -> t
  _OR :: Foldable f => f t -> t
  fAll :: (Foldable f, Functor f) => f e -> (e -> t) -> t
  exst :: (Foldable f, Functor f) => f e -> (e -> t) -> t
  (===>) :: t -> t -> t
  (<===) :: t -> t -> t
  (<==>) :: t -> t -> t
  
  _AND xs = foldl (/\) verum xs
  _OR xs = foldl (\/) falsum xs
  fAll es p = _AND $ p <$> es
  exst es p = _OR $ p <$> es
  a ===> b = (neg a) \/ b
  a <=== b = b ===> a
  (<==>) a b = (a ===> b) /\ (b ===> a)
  (/\) a b = neg $ (neg a) \/ (neg b)
  (\/) a b = neg $ (neg a) /\ (neg b)
  verum = neg falsum
  falsum = neg verum
     
data NExpr (t :: NType) where
  BoolLiteral :: Bool -> NExpr NBool
  IntLiteral :: Int -> NExpr NInt
  EnumLiteral :: String -> NExpr NEnum
  Ref :: [String] -> NExpr t
  LocalParameter :: String -> NExpr t
  NAbs :: NExpr NInt -> NExpr NInt
  NMax :: NExpr NInt -> NExpr NInt -> NExpr NInt
  NMin :: NExpr NInt -> NExpr NInt -> NExpr NInt
  NPlus :: NExpr NInt -> NExpr NInt -> NExpr NInt
  NMult :: NExpr NInt -> NExpr NInt -> NExpr NInt
  NMinus :: NExpr NInt -> NExpr NInt -> NExpr NInt
  NLt :: NExpr NInt -> NExpr NInt -> NExpr NBool
  NGt :: NExpr NInt -> NExpr NInt -> NExpr NBool
  NLte :: NExpr NInt -> NExpr NInt -> NExpr NBool
  NGte :: NExpr NInt -> NExpr NInt -> NExpr NBool
  NEq :: NExpr tpe -> NExpr tpe -> NExpr NBool
  NIEq :: NExpr tpe -> NExpr tpe -> NExpr NBool
  NCount :: [NExpr NBool] -> NExpr NInt
  NextExpr :: NExpr t -> NExpr t
  NDCaseExpr :: [(NExpr NBool, NExpr (NSet t))] -> NExpr t
  CaseExpr :: [(NExpr NBool, NExpr t)] -> NExpr t
  ITE :: NExpr NBool -> NExpr tpe -> NExpr tpe -> NExpr tpe
  SetExpr :: [NExpr t] -> NExpr (NSet t)
  InSetExpr :: NExpr t -> NExpr (NSet t) -> NExpr NBool
  ArrayIndex :: Int -> NExpr (NArray t) -> NExpr t
  SAnd :: NExpr NBool -> NExpr NBool -> NExpr NBool
  SOr :: NExpr NBool -> NExpr NBool -> NExpr NBool
  SNot :: NExpr NBool -> NExpr NBool

instance Eq (NExpr (t :: NType)) where
  BoolLiteral a == BoolLiteral b = a == b
  IntLiteral a == IntLiteral b = a == b
  EnumLiteral a == EnumLiteral b = a == b
  Ref a == Ref b = a == b
  LocalParameter a == LocalParameter b = a == b
  NAbs a == NAbs b = a == b
  NMax a b == NMax c d = (a == c && b == d) || (a == d && b == c)
  NMin a b == NMin c d = (a == c && b == d) || (a == d && b == c)
  NPlus a b == NPlus c d = (a == c && b == d) || (a == d && b == c)
  NMult a b == NMult c d = (a == c && b == d) || (a == d && b == c)
  NMinus a b == NMinus c d = a == c && b == d
  NLt a b == NLt c d = a == c && b == d
  NGt a b == NGt c d = a == c && b == d
  NLte a b == NLte c d = a == c && b == d
  NGte a b == NGte c d = a == c && b == d
  --NEq a b == NEq c d = (a == c && b == d) || (a == d && b == c)
  --NIEq a b == NIEq c d = (a == c && b == d) || (a == d && b == c)
  NCount a == NCount b = a == b
  NextExpr a == NextExpr b = a == b
  NDCaseExpr a == NDCaseExpr b = a == b
  CaseExpr a == CaseExpr b = a == b
  ITE a b c == ITE d e f = a == d && b == e && c == f
  SetExpr a == SetExpr b = a == b
  --InSetExpr a b == InSetExpr c d = (a == c) && b == d
  ArrayIndex a b == ArrayIndex c d = a == c && b == d
  SAnd a b == SAnd c d = (a == c && b == d) || (a == d && b == c)
  SOr a b == SOr c d = (a == c && b == d) || (a == d && b == c)
  SNot a == SNot b = a == b
  _ == _ = False

instance Ord (NExpr (t :: NType)) where
  BoolLiteral a <= BoolLiteral b = a <= b
  IntLiteral a <= IntLiteral b = a <= b
  EnumLiteral a <= EnumLiteral b = a <= b
  Ref a <= Ref b = a <= b
  LocalParameter a <= LocalParameter b = a <= b
  NAbs a <= NAbs b = a <= b
  NMax a b <= NMax c d = (a <= c && b <= d) || (a <= d && b <= c)
  NMin a b <= NMin c d = (a <= c && b <= d) || (a <= d && b <= c)
  NPlus a b <= NPlus c d = (a <= c && b <= d) || (a <= d && b <= c)
  NMult a b <= NMult c d = (a <= c && b <= d) || (a <= d && b <= c)
  NMinus a b <= NMinus c d = a <= c && b <= d
  NLt a b <= NLt c d = a <= c && b <= d
  NGt a b <= NGt c d = a <= c && b <= d
  NLte a b <= NLte c d = a <= c && b <= d
  NGte a b <= NGte c d = a <= c && b <= d
  --NEq a b <= NEq c d = (a <= c && b <= d) || (a <= d && b <= c)
  --NIEq a b <= NIEq c d = (a <= c && b <= d) || (a <= d && b <= c)
  NCount a <= NCount b = a <= b
  NextExpr a <= NextExpr b = a <= b
  NDCaseExpr a <= NDCaseExpr b = a <= b
  CaseExpr a <= CaseExpr b = a <= b
  ITE a b c <= ITE d e f = a <= d && b <= e && c <= f
  SetExpr a <= SetExpr b = a <= b
  --InSetExpr a b <= InSetExpr c d = (a <= c) && b <= d
  ArrayIndex a b <= ArrayIndex c d = a <= c && b <= d
  SAnd a b <= SAnd c d = (a <= c && b <= d) || (a <= d && b <= c)
  SOr a b <= SOr c d = (a <= c && b <= d) || (a <= d && b <= c)
  SNot a <= SNot b = a <= b
  _ <= _ = False

(===) :: NExpr t -> NExpr t -> NExpr NBool
(===) = NEq

class Literable t l | t -> l where
  lit :: t -> NExpr l
instance Literable Bool NBool where lit = BoolLiteral
instance Literable Int NInt where lit = IntLiteral
instance Literable String NEnum where lit = EnumLiteral
instance Literable [NExpr t] (NSet t) where lit = SetExpr

instance Prop (NExpr NBool) where
  verum = BoolLiteral True
  falsum = BoolLiteral False
  neg (BoolLiteral True) = falsum
  neg (BoolLiteral False) = verum
  neg (NEq a b) = NIEq a b
  neg (SAnd a b) = (neg a) \/ (neg b)
  neg (SOr a b) = (neg a) /\ (neg b)
  neg e = SNot e
  (BoolLiteral True) /\ b = b
  a /\ (BoolLiteral True) = a
  (BoolLiteral False) /\ _ = falsum
  _ /\ (BoolLiteral False) = falsum
  a /\ b = SAnd a b
  (BoolLiteral True) \/ _ = verum
  _ \/ (BoolLiteral True) = verum
  (BoolLiteral False) \/ b = b
  a \/ (BoolLiteral False) = a
  a \/ b = SOr a b

instance Num (NExpr NInt) where
  (+) = NPlus
  (-) = NMinus
  (*) = NMult
  abs = NAbs
  signum i = NMin (IntLiteral 1) $ NMax (IntLiteral $ -1) i
  fromInteger = IntLiteral . fromInteger

instance Embeddable Bool (NExpr NBool) where embed = BoolLiteral

data Range t where
  IntRange :: { lowerBound :: Int, upperBound :: Int } -> Range NInt
  BoolRange :: Range NBool
  EnumRange :: { values :: [String] } -> Range NEnum
  ArrayRange :: { lowerIndex :: Int, upperIndex :: Int, tpe :: Range t } -> Range (NArray t)
  ModuleInstance :: a -> Module a -> Range NModule

emptyRange :: Range t -> Bool
emptyRange BoolRange = False
emptyRange (ModuleInstance _ _) = False
emptyRange (IntRange l u) = l > u
emptyRange (EnumRange xs) = null xs
emptyRange (ArrayRange l u t) = l > u || emptyRange t

singletonRange :: Range t -> Bool
singletonRange BoolRange = False
singletonRange (ModuleInstance _ _) = False --it may be but for our cause it isnt
singletonRange (IntRange l u) = l == u
singletonRange (EnumRange xs) = length xs == 1
singletonRange (ArrayRange l u t) = False --I dont think NuSMV optimizes arrays... l == u && singletonRange t


uniteRange :: Range NInt -> Range NInt -> Range NInt
uniteRange (IntRange lower upper) (IntRange x y) = IntRange (min lower x) (max upper y)

extendToFit :: Range NInt -> Int -> Range NInt
extendToFit (IntRange lower upper) y = IntRange (min lower y) (max upper y)

lowerVal :: Range NInt -> Int
lowerVal (IntRange lower _) = lower - 1
greaterVal :: Range NInt -> Int
greaterVal (IntRange _ upper) = upper + 1

data Variable t = CVar { name :: String, rng :: Range t }
                | IVar { name :: String, rng :: Range t }
                | Frozen { name :: String, rng :: Range t }
                | Constant { name :: String }
                | Definition { name :: String, body :: NExpr t } -- can't mention future


class Referenceable f (t :: NType) | f -> t where
  ref :: f -> NExpr t --can't future ref
  refIn :: [Variable NModule] -> f -> NExpr t

  ref = refIn []

instance Referenceable (Variable t) t where refIn pref v = Ref $ (name <$> pref) ++ [name v]

defaultCase :: Variable t -> (NExpr NBool, NExpr t)
defaultCase v = (embed True, ref v)

defaultFCase :: Variable t -> (NExpr NBool, NExpr t)
defaultFCase v = (NextExpr $ embed True, ref v)

data Constraint = Justice (NExpr NBool)
                | Fairness (NExpr NBool)
                | Compassion (NExpr NBool) (NExpr NBool)
                | Trans (NExpr NBool)
                | Init (NExpr NBool)
                | Invar (NExpr NBool)

data PropLogic t = Simple (NExpr NBool)
            | Braced t
            | PNot t
            | PAnd t t
            | POr t t
            | PXor t t
            | PXnor t t
            | PImp t t
            | PEquiv t t

instance Embeddable (NExpr NBool) (PropLogic t) where embed = Simple

data CTL = CP (PropLogic CTL) | EG CTL | EX CTL | EF CTL | AG CTL | AX CTL | AF CTL | EU CTL CTL | AU CTL CTL
instance Embeddable (PropLogic CTL) CTL where embed = CP
instance Embeddable (NExpr NBool) CTL where embed = CP . Simple
instance Embeddable Bool CTL where embed = CP . Simple . BoolLiteral
instance Prop CTL where
  neg (CP (Simple a)) = embed $ neg a
  neg (CP (Braced a)) = embed $ Braced $ neg a
  neg (CP (PNot a)) = embed $ a
  neg (CP (PAnd a b)) = embed $ (neg a) \/ (neg b)
  neg (CP (POr a b)) = embed $ (neg a) /\ (neg b)
  neg (CP (PImp a b)) = embed $ a /\ (neg b)
  neg (CP (PEquiv a b)) = embed $ a <==> (neg b)
  neg a = embed $ PNot a
  (CP (Simple (BoolLiteral True))) /\ b = b
  a /\ (CP (Simple (BoolLiteral True))) = a
  (CP (Simple (BoolLiteral False))) /\ _ = falsum
  _ /\ (CP (Simple (BoolLiteral False))) = falsum
  a /\ b = embed $ PAnd a b
  (CP (Simple (BoolLiteral True))) \/ _ = verum
  _ \/ (CP (Simple (BoolLiteral True))) = verum
  (CP (Simple(BoolLiteral False))) \/ b = b
  a \/ (CP (Simple (BoolLiteral False))) = a
  a \/ b = embed $ POr a b
  falsum = embed $ lit False
  verum = embed $ lit True
  (===>) (CP (Simple (BoolLiteral True))) b = b
  (===>) (CP (Simple (BoolLiteral False))) _ = verum
  (===>) _ (CP (Simple (BoolLiteral True))) = verum
  (===>) a (CP (Simple (BoolLiteral False))) = neg a
  (===>) a b = embed $ PImp a b
  (<==>) (CP (Simple (BoolLiteral True))) b = b
  (<==>) (CP (Simple (BoolLiteral False))) b = neg b
  (<==>) a (CP (Simple (BoolLiteral True))) = a
  (<==>) a (CP (Simple (BoolLiteral False))) = neg a
  (<==>) a b = embed $ PEquiv a b

  
ar :: CTL -> CTL -> CTL
ar a b = neg $ EU (neg a) (neg b)
er :: CTL -> CTL -> CTL
er a b = neg $ AU (neg a) (neg b)
aw :: CTL -> CTL -> CTL
aw a b = ar b (a \/ b)
ew :: CTL -> CTL -> CTL
ew a b = er b (a \/ b)

data LTL = LP (PropLogic LTL) | X LTL | G LTL | GB Int Int LTL | F LTL | FB Int Int LTL | U LTL LTL | V LTL LTL | Y LTL | Z LTL | H LTL | HB Int Int LTL | O LTL | OB Int Int LTL | S LTL LTL | T LTL LTL
instance Embeddable (PropLogic LTL) LTL where embed = LP
instance Embeddable (NExpr NBool) LTL where embed = LP . Simple
instance Embeddable Bool LTL where embed = LP . Simple . BoolLiteral
instance Prop LTL where
  neg (LP (Simple a)) = embed $ neg a
  neg (LP (Braced a)) = embed $ Braced $ neg a
  neg (LP (PNot a)) = embed $ a
  neg (LP (PAnd a b)) = embed $ (neg a) \/ (neg b)
  neg (LP (POr a b)) = embed $ (neg a) /\ (neg b)
  neg (LP (PImp a b)) = embed $ a /\ (neg b)
  neg (LP (PEquiv a b)) = embed $ a <==> (neg b)
  neg a = embed $ PNot a
  (LP (Simple (BoolLiteral True))) /\ b = b
  a /\ (LP (Simple (BoolLiteral True))) = a
  (LP (Simple (BoolLiteral False))) /\ _ = falsum
  _ /\ (LP (Simple (BoolLiteral False))) = falsum
  a /\ b = embed $ PAnd a b
  (LP (Simple (BoolLiteral True))) \/ _ = verum
  _ \/ (LP (Simple (BoolLiteral True))) = verum
  (LP (Simple(BoolLiteral False))) \/ b = b
  a \/ (LP (Simple (BoolLiteral False))) = a
  a \/ b = embed $ POr a b
  falsum = embed $ lit False
  verum = embed $ lit True
  (===>) (LP (Simple (BoolLiteral True))) b = b
  (===>) (LP (Simple (BoolLiteral False))) _ = verum
  (===>) _ (LP (Simple (BoolLiteral True))) = verum
  (===>) a (LP (Simple (BoolLiteral False))) = neg a
  (===>) a b = embed $ PImp a b
  (<==>) (LP (Simple (BoolLiteral True))) b = b
  (<==>) (LP (Simple (BoolLiteral False))) b = neg b
  (<==>) a (LP (Simple (BoolLiteral True))) = a
  (<==>) a (LP (Simple (BoolLiteral False))) = neg a
  (<==>) a b = embed $ PEquiv a b

w :: LTL -> LTL -> LTL
w a b = U a b \/ G a

data Spec = CTLSpec (Maybe String) CTL | LTLSpec (Maybe String) LTL
namedCTLSpec :: String -> CTL -> Spec
namedCTLSpec n f = CTLSpec (Just n) f
namedLTLSpec :: String -> LTL -> Spec
namedLTLSpec n f = LTLSpec (Just n) f
         
                           
data Assign where
  InitAss :: { _var :: Variable t, expr :: NExpr t } -> Assign
  NextAss :: { _var :: Variable t, expr :: NExpr t } -> Assign --complex missing for now
  ArrayInitAss :: { _avar :: Variable (NArray t), exps :: [NExpr t] } -> Assign
  ArrayNextAss :: { _avar :: Variable (NArray t), exps :: [NExpr t] } -> Assign

data Module a = Module { _modname :: String
                       , _params :: [String]
                       , _boolMembers :: [Variable NBool]
                       , _intMembers :: [Variable NInt]
                       , _enumMembers :: [Variable NEnum]
                       , _moduleMembers :: [Variable NModule]
                       , _esetMembers :: [Variable (NSet NEnum)]
                       , _arrBoolMembers :: [Variable (NArray NBool)]
                       , _assigns :: [Assign]
                       , _constraints :: [Nusmv.Constraint]
                       , _spec :: [Spec]
                       , _extractors :: [a -> String]
                       , _nameSchema :: a -> String
                       }
makeLenses ''Module

emptyModule :: String -> (a -> String) -> Module a
emptyModule n f = Module n [] [] [] [] [] [] [] [] [] [] [] f

specModule :: String -> (a -> String) -> [Spec] -> Module a
specModule n f s = Module n [] [] [] [] [] [] [] [] [] s [] f
   
class Addable t u where 
  add :: t -> Module u -> Module u
  addAll :: Foldable f => f t -> Module u -> Module u
  
  addAll ts mo = foldr (add) mo ts
class Varable (f :: NType -> Type) where 
  var :: f t -> Variable t
class Assignable t where
  ass :: t -> [Assign]

data MemberDef t u where
  MemberDef :: Literable u v => { mname :: String, extractor :: t -> u } -> MemberDef t v
  AutoInput :: Referenceable r v => { mname :: String, extractor :: t -> r } -> MemberDef t v
  DirectInput :: { mname :: String, ext :: t -> NExpr v } -> MemberDef t v
derive :: (b -> a) -> MemberDef a t -> MemberDef b t
derive f (MemberDef name extractor) = (MemberDef name (extractor . f))
derive f (AutoInput name extractor) = (AutoInput name (extractor . f))
derive f (DirectInput name extractor) = (DirectInput name (extractor . f))

data StateVar t = StateVar { varV :: Variable t, _sinit :: NExpr t, _snext :: Variable t -> NExpr t }
data ArrayVar t = ArrayVar { avarV :: Variable (NArray t), _ainit :: [NExpr t], _anext :: Variable (NArray t) -> [NExpr t] }

data OutsideState (t :: NType) = OutsideState { osname :: String, path :: [String] }

instance Varable (MemberDef t) where 
  var (MemberDef name _) = Definition name $ LocalParameter $ "p_" ++ name 
  var (AutoInput name _) = Definition name $ LocalParameter $ "p_" ++ name
  var (DirectInput name _) = Definition name $ LocalParameter $ "p_" ++ name
instance Varable StateVar where var (StateVar res _ _) = res

instance Assignable (StateVar t) where
  ass s = if singletonRange $ rng $ var s then [] else [InitAss (var s) $ _sinit s, NextAss (var s) $ _snext s $ var s] 
instance Assignable (ArrayVar t) where ass s = [ArrayInitAss (avarV s) $ _ainit s, ArrayNextAss (avarV s) $ _anext s $ avarV s]

addableMDT :: (NShow (NExpr t)) => Lens' (Module u) [Variable t] -> MemberDef u t -> Module u -> Module u
addableMDT l md m = case md of
                      (MemberDef name ext) -> over extractors (++ [nshow . lit . ext]) $ over l (++ [var md]) $ over params (++ ["p_" ++ name]) m
                      (AutoInput name ext) -> over extractors (++ [nshow . ref . ext]) $ over l (++ [var md]) $ over params (++ ["p_" ++ name]) m
                      (DirectInput name ext) -> over extractors (++ [nshow . ext]) $ over l (++ [var md]) $ over params (++ ["p_" ++ name]) m
                                       
instance (NShow (NExpr NBool)) => Addable (MemberDef u NBool) u where add = addableMDT boolMembers   
instance (NShow (NExpr NEnum)) => Addable (MemberDef u NEnum) u where add = addableMDT enumMembers
instance (NShow (NExpr NInt)) => Addable (MemberDef u NInt) u where add = addableMDT intMembers
instance (NShow (NExpr (NSet NEnum))) => Addable (MemberDef u (NSet NEnum)) u where add = addableMDT esetMembers
instance (NShow (NExpr (NArray NBool))) => Addable (MemberDef u (NArray NBool)) u where add = addableMDT arrBoolMembers

addableSVT :: (Assignable (StateVar t), Varable StateVar) => Lens' (Module u) [Variable t] -> StateVar t -> Module u -> Module u
addableSVT l sv m = over assigns (++ ass sv) $ over l (++ [var sv]) m

instance Addable (StateVar NBool) u where add = addableSVT boolMembers
instance Addable (StateVar NInt) u where add = addableSVT intMembers
instance Addable (StateVar NEnum) u where add = addableSVT enumMembers

instance Addable (OutsideState t) u where 
  add (OutsideState nme pth) m = over extractors (++ [const $ concat $ intersperse "." pth]) $ over params (++ [nme]) m

instance Addable (ArrayVar NBool) u where
  add av@(ArrayVar v _ _) = case rng v of
                           ArrayRange t u r | t > u -> over enumMembers (++[Constant (name v)])
                           _ -> over assigns (++ ass av) . (over arrBoolMembers (++ [avarV av]))

instance Addable (Variable NBool) u where
  add v = over boolMembers (++ [v])

modVarName :: Module t -> t -> String
modVarName m val = view nameSchema m $ val
  
instance Referenceable (StateVar tpe) tpe where refIn pref sv = refIn pref $ var sv
instance Referenceable (MemberDef t tpe) tpe where refIn pref sv = refIn pref $ var sv
instance Referenceable (ArrayVar tpe) (NArray tpe) where refIn pref sv = refIn pref $ avarV sv
instance Referenceable (OutsideState tpe) tpe where 
  ref (OutsideState nme _) = LocalParameter nme
  refIn pref (OutsideState _ pth) = Ref pth

inst :: Module t -> t -> Variable NModule
inst mod val = CVar (modVarName mod val) $ ModuleInstance val mod


type Property t u = t -> NExpr u
is :: (Referenceable r t, Literable l t, Embeddable (NExpr NBool) o) => r -> l -> o
is a b = embed $ (ref a) === (lit b)

ins :: (Referenceable r t, Embeddable (NExpr NBool) o) => r -> NExpr (NSet t) -> o
ins a s = embed $ InSetExpr (ref a) s

exactly :: Int -> [t] -> Property t NBool -> NExpr NBool
exactly l elements prop = NEq (NCount $ prop <$> elements) (lit l)

search :: Property t NBool ->  Nel.NonEmpty t -> Property t u -> NExpr u
search sel xs prop = CaseExpr $ Nel.toList $ (\x -> (sel x, prop x)) <$> xs

search' :: NExpr u -> Property t NBool -> [t] -> Property t u -> NExpr u
search' defaul _ [] _ = defaul
search' _ sel (x : xs) prop = search sel (x Nel.:| xs) prop

enumRange :: Literable t NEnum => Nel.NonEmpty t -> Range NEnum
enumRange xs = EnumRange $ Nel.toList $ nshow . lit <$> xs



tshow :: (NShow t, NShow u) => (t, u) -> String
tshow (c, r) = nshow c ++ " : " ++ nshow r ++ ";"

instance NShow (NExpr t) where
  nshow (BoolLiteral x) = if x then "TRUE" else "FALSE"
  nshow (IntLiteral x) = show x
  nshow (EnumLiteral x) = x
  nshow (Ref x) = concat $ intersperse "." x
  nshow (NAbs x) = "abs(" ++ nshow x ++ ")" 
  nshow (NMax x y) = "max(" ++ nshow x ++ ", " ++ nshow y ++ ")"
  nshow (NMin x y) = "min(" ++ nshow x ++ ", " ++ nshow y ++ ")"
  nshow (NPlus x y) =  "(" ++ nshow x ++ " + " ++ nshow y ++ ")"
  nshow (NMinus x y) = "(" ++ nshow x ++ " - " ++ nshow y ++ ")"
  nshow (NMult x y) = "(" ++ nshow x ++ " * " ++ nshow y ++ ")"
  nshow (NLt x y) = "(" ++ nshow x ++ " < " ++ nshow y ++ ")"
  nshow (NGt x y) = "(" ++ nshow x ++ " > " ++ nshow y ++ ")"
  nshow (NLte x y) = "(" ++ nshow x ++ " <= " ++ nshow y ++ ")"
  nshow (NGte x y) = "(" ++ nshow x ++ " >= " ++ nshow y ++ ")"
  nshow (NEq x y) = "(" ++ nshow x ++ " = " ++ nshow y ++ ")"
  nshow (NIEq x y) = "(" ++ nshow x ++ " != " ++ nshow y ++ ")"
  nshow (NextExpr n) = "next(" ++ nshow n ++ ")"
  nshow (NCount xs) = if (null xs) then "0" else "count(" ++ (intercalate ", " $ nshow <$> xs) ++ ")"
  nshow (CaseExpr xs) = "case\n  " ++ (intercalate "\n  " $ tshow <$> xs) ++ "\nesac"
  nshow (SetExpr xs) = "{" ++ (intercalate ", " $ nshow <$> xs) ++ "}"
  nshow (SAnd x y) = "(" ++ nshow x ++ " & " ++ nshow y ++ ")"
  nshow (SOr x y) = "(" ++ nshow x ++ " | " ++ nshow y ++ ")"
  nshow (SNot x) = "!(" ++ nshow x ++ ")" 
  nshow (InSetExpr x y) = "(" ++ nshow x ++ " in " ++ nshow y ++ ")"
  nshow (ArrayIndex i x) = nshow x ++ "[" ++ show i ++ "]"
  nshow (LocalParameter x) = x
  nshow (NDCaseExpr xs) = "case\n" ++ (intercalate "\n  " $ tshow <$> xs) ++ "\nesac"
  nshow (ITE b x y) = "(" ++ nshow b ++ " ? " ++ nshow x ++ " : " ++ nshow y ++ ")"

instance NShow l => NShow (PropLogic l) where
  nshow (Simple expr) = nshow expr
  nshow (Braced content) = "(" ++ nshow content ++ ")"
  nshow (PNot content) = "!(" ++ nshow content ++ ")"
  nshow (PAnd x y) = "(" ++ nshow x ++ " & " ++ nshow y ++ ")"
  nshow (POr x y) = "(" ++ nshow x ++ " | " ++ nshow y ++ ")"
  nshow (PXor x y) = "(" ++ nshow x ++ " xor " ++ nshow y ++ ")" 
  nshow (PXnor x y) = "(" ++ nshow x ++ " xnor " ++ nshow y ++ ")"
  nshow (PImp antecedent consequent) = "(" ++ nshow antecedent ++ " -> " ++ nshow consequent ++ ")"
  nshow (PEquiv x y) = "(" ++ nshow x ++ " <-> " ++ nshow y ++ ")"

instance NShow CTL where
  nshow (CP x) = nshow x
  nshow (EG x) = "EG (" ++ nshow x ++ ")"
  nshow (EX x) = "EX (" ++ nshow x ++ ")"
  nshow (EF x) = "EF (" ++ nshow x ++ ")"
  nshow (AG x) = "AG (" ++ nshow x ++ ")"
  nshow (AX x) = "AX (" ++ nshow x ++ ")"
  nshow (AF x) = "AF (" ++ nshow x ++ ")"
  nshow (EU x y) = "E[ " ++ nshow x ++ " U " ++ nshow y ++ " ]"
  nshow (AU x y) = "A[ " ++ nshow x ++ " U " ++ nshow y ++ " ]"
  
instance NShow LTL where
  nshow (LP x) = nshow x
  nshow (Z x) = "Z (" ++ nshow x ++ ")"  
  nshow (X x) = "X (" ++ nshow x ++ ")"
  nshow (G x) = "G (" ++ nshow x ++ ")"
  nshow (GB l u y) = "G (" ++ show l ++ ".." ++ show u ++ ") (" ++ nshow y ++ ")"
  nshow (F x) = "F (" ++ nshow x ++ ")"
  nshow (FB l u y) = "F (" ++ show l ++ ".." ++ show u ++ ") (" ++ nshow y ++ ")"
  nshow (U x y) = "(" ++ nshow x ++ " U " ++ nshow y ++ ")"
  nshow (V x y) = "(" ++ nshow x ++ " V " ++ nshow y ++ ")"
  nshow (Y x) = "Y (" ++ nshow x ++ ")"
  nshow (H x) = "H (" ++ nshow x ++ ")"
  nshow (HB l u y) = "H (" ++ show l ++ ".." ++ show u ++ ") (" ++ nshow y ++ ")"
  nshow (O x) = "O (" ++ nshow x ++ ")"
  nshow (OB l u y) = "O (" ++ show l ++ ".." ++ show u ++ ") (" ++ nshow y ++ ")"
  nshow (S x y) = "(" ++ nshow x ++ " S " ++ nshow y ++ ")"
  nshow (T x y) = "(" ++ nshow x ++ " T " ++ nshow y ++ ")"


instance NShow (Range t)  where
  nshow (IntRange lower upper) = show lower ++ ".." ++ show upper
  nshow BoolRange = "boolean"
  nshow (EnumRange values) = "{ " ++ (intercalate ", " values) ++ " }"
  nshow (ModuleInstance val (Module name _ _ _ _ _ _ _ _ _ _ extract _)) = name ++ "(" ++ (intercalate ", " params) ++ ")" 
    where 
      params :: [String]
      params = (extract <*> (pure val))
  nshow (ArrayRange lo up tpe) = "array " ++ show lo ++ ".." ++ show up ++ " of " ++ (nshow tpe)
  
  
instance NShow Nusmv.Constraint where 
  nshow (Justice x) = "JUSTICE " ++ nshow x ++ ";"
  nshow (Fairness x) = "FAIRNESS " ++ nshow x ++ ";"
  nshow (Compassion x y) = "COMPASSION (" ++ nshow x ++ ", " ++ nshow y ++");"
  nshow (Invar n) = "INVAR " ++ nshow n ++ ";"
  nshow (Init n) = "INIT " ++ nshow n ++ ";"
  nshow (Trans n) = "TRANS " ++ nshow n ++ ";"
  
instance NShow (Variable t) where
  nshow (CVar name rng) = name ++ " : " ++ nshow rng ++ ";"
  nshow (IVar name rng) = name ++ " : " ++ nshow rng ++ ";"
  nshow (Frozen name rng) = name ++ " : " ++ nshow rng ++ ";"
  nshow (Constant name) = name
  nshow (Definition name body) = name ++ " := " ++ nshow body ++ ";"
  
maybeName :: Maybe String -> String
maybeName Nothing = ""
maybeName (Just x) = "NAME " ++ x ++ " := "
  
instance NShow Spec where
  nshow (CTLSpec mn x) = "CTLSPEC " ++ maybeName mn ++ nshow x ++ ";"
  nshow (LTLSpec mn x) = "LTLSPEC " ++ maybeName mn ++ nshow x ++ ";"
  
  
nshow_vars :: [Variable t] -> ([String], ([String], ([String], ([String], [String]))))
nshow_vars [] = ([], ([], ([], ([], []))))
nshow_vars (x : xs) = case x of
                           (CVar _ _) ->       (nshow x : vars, (ivars, (frozen, (const, def))))
                           (IVar _ _) ->       (vars, (nshow x : ivars, (frozen, (const, def))))
                           (Frozen _ _) ->     (vars, (ivars, (nshow x : frozen, (const, def))))
                           (Constant _) ->     (vars, (ivars, (frozen, (nshow x : const, def))))
                           (Definition _ _) -> (vars, (ivars, (frozen, (const, nshow x : def))))
  where 
    rest :: ([String], ([String], ([String], ([String], [String]))))
    rest = nshow_vars xs
    vars :: [String]
    vars = fst rest
    ivars :: [String]
    ivars = fst $ snd rest
    frozen :: [String]
    frozen = fst $ snd $ snd rest
    const :: [String]
    const = fst $ snd $ snd $ snd rest
    def :: [String]
    def = snd $ snd $ snd $ snd rest


initTemplate :: String -> String -> String
initTemplate e v = "init(" ++ e ++ ") := " ++ v ++ ";"
nextTemplate :: String -> String -> String
nextTemplate e v = "next(" ++ e ++ ") := " ++ v ++ ";"
indexMakerHappener :: (String -> String -> String) -> Variable (NArray t) -> [NExpr t] -> [String]
indexMakerHappener template v bs = zipWith (\i -> \e -> template (nshow $ ArrayIndex i (ref v)) $ nshow e) [0..] bs
instance NShow Assign where
  nshow (InitAss x y) = initTemplate (name x) (nshow y)
  nshow (NextAss x y) = nextTemplate (name x) (nshow y)
  nshow (ArrayInitAss x y) = unlines $ indexMakerHappener initTemplate x y
  nshow (ArrayNextAss x y) = unlines $ indexMakerHappener nextTemplate x y
  
section :: String -> [String] -> [String]
section name body = if length body > 0 then name : (("  " ++) <$> (body >>= lines)) else []
  

instance NShow (Module t) where
  nshow (Module nam params bools ints enums modules esets barr assigns constraints specs _ _) = 
    let (vars, (ivars, (frozen, (const, def)))) = (nshow_vars $ bools) <> (nshow_vars $ ints) <> (nshow_vars $ enums) <> (nshow_vars $ modules) <> (nshow_vars $ esets) <> (nshow_vars $ barr)
        ass = nshow <$> assigns 
        paramList = intercalate ", " params in 
    unlines $ (("MODULE " ++ nam ++ "(" ++ paramList ++ ")") :
    (section "VAR"  vars) ++
    (section "IVAR" ivars) ++
    (section "FROZENVAR" frozen) ++ 
    (section "CONSTANTS" $ filter (";" /=) [(intercalate ", " const) ++ ";"]) ++ 
    (section "DEFINE" def) ++ 
    (section "ASSIGN" $ withStrategy (parList rpar) ass) ++ 
    (nshow <$> constraints) ++ 
    (nub $ nshow <$> specs)
    )

