module Export where

class Exportable t where
  exportNuSMV :: t -> [String]
  exportCOOLModel :: t -> [String]
  exportCOOLSpec :: t -> [String]
