{-# LANGUAGE DerivingStrategies, GeneralisedNewtypeDeriving #-}

module COOL where

import Data.Map
import Data.List

type State = String
type Atoms = [String]

newtype KModel = KModel (Map State (Atoms, [State]))
  deriving stock (Eq, Ord)
  deriving newtype (Semigroup, Monoid)

instance Show KModel where
  show (KModel s) = unlines $ (\ (st, (atoms, succs)) -> st <> " : ({" <> intercalate ", " atoms <> "}, {" <> intercalate ", " (showSucc <$> succs) <> "})") <$> assocs s
    where
      showSucc st = "({}, " <> st <> ")"
