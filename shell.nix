{ nixpkgs ? import <nixpkgs> {}, compiler ? "default", doBenchmark ? false }:

let

  inherit (nixpkgs) pkgs;

  f = { mkDerivation, aeson, base, bytestring, containers, fgl
      , lens, lib, MissingH, parallel, parsec, scientific, text
      }:
      mkDerivation {
        pname = "hsmv";
        version = "0.1.0.0";
        src = ./.;
        isLibrary = true;
        isExecutable = true;
        libraryHaskellDepends = [
          aeson base bytestring containers fgl lens MissingH parallel parsec
          scientific text
        ];
        executableHaskellDepends = [ aeson base bytestring fgl lens ];
        testHaskellDepends = [ base ];
        homepage = "https://gitlab.com/mgttlinger/hsmv";
        license = lib.licenses.unfree;
        hydraPlatforms = lib.platforms.none;
        mainProgram = "hsmv-exe";
      };

  haskellPackages = if compiler == "default"
                       then pkgs.haskellPackages
                       else pkgs.haskell.packages.${compiler};

  variant = if doBenchmark then pkgs.haskell.lib.doBenchmark else pkgs.lib.id;

  drv = variant (haskellPackages.callPackage f {});

in

  if pkgs.lib.inNixShell then drv.env else drv
