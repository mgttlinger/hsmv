{-# LANGUAGE ImplicitParams #-}

module Main where

import Nusmv
import Osek
import Export
import OsekExport
import OsekImport
import Data.Aeson
import qualified Data.ByteString.Lazy as B
import qualified Data.Graph.Inductive as G
import System.Environment
import Control.Lens
import qualified Data.List.NonEmpty as Nel
import Data.List
import Control.Arrow
import Data.Maybe

decodeExc :: MonadFail e => Either String a -> e a
decodeExc (Left s) = fail s
decodeExc (Right v) = pure v

freshABBsEliminated :: MonadFail m => StaticConf -> Runtime -> m ()
freshABBsEliminated sc rt = let ?sc = sc
                                ?os = rt
                            in
                              if elem "fresh" runnableABBs then fail "there was an empty next-block that could not be eliminated" else pure () 
                          
checkIfDeterministic :: MonadFail m => Runtime -> m ()
checkIfDeterministic os = if isDeterministic $ _graph os then pure () else fail "Graph is not deterministic"
  where
    isDeterministic :: Eq e => G.Gr n e -> Bool
    isDeterministic g = all differentEdgeLabels (G.nodes g)
      where
        differentEdgeLabels :: G.Node -> Bool
        differentEdgeLabels node =
          let outEdges = G.out g node
          in length outEdges == length (nub (G.edgeLabel <$> outEdges))


systemPrinter :: StaticConf -> Runtime -> String
systemPrinter sc os = let i1s = _isr1s sc
                          i2s = _isr2s sc
                          tas = _tasks sc
                          evs = _events sc
                          res = _resources sc
                      in
                        unlines [ ""
                                , "SystemInfo:"
                                , intercalate "/" [show $ (length i1s) + (length i2s), show (length tas), show (length evs), show (length res)] ++ " & " ++ (intercalate ", " $ sort $ ruinfo os <$> (Nel.filter (("Idle" /=) . runame) $ runnables sc))
                                , ""
                                ]
  where
    neverActivated :: Runtime -> Runnable -> Bool
    neverActivated os ru = not $ (uncurry (||)) $ ((Ready `elem`) &&& (Running `elem`)) $ Nel.toList $ stateAt ru <$> states os
    braced s = "(" ++ s ++ ")"
    ruinfo :: Runtime -> Runnable -> String
    ruinfo os ru = (if neverActivated os ru then braced else id) $ runame ru ++ ":~" ++ (show $ ruprio ru) ++ (if taskAndPreemptable ru then "~P" else "")
    taskAndPreemptable (ET x) = _preemptable x
    taskAndPreemptable _ = False

prioScaling :: StaticConf -> StaticConf
prioScaling sc = over tasks (map $ tprio *~ 2) $
  over isr1s (map $ i1prio *~ 2) $
  over isr2s (map $ i2prio *~ 2) sc



collectInputs :: [String] -> IO (StaticConf, Runtime)
collectInputs [oilFile, graphFile] = do
  jsonGraphFile <- B.readFile graphFile 
  jsonOilFile <- B.readFile oilFile
  sc <- decodeExc $ eitherDecode jsonOilFile
  sc2Rt <- decodeExc $ eitherDecode jsonGraphFile
  (s, r) <- decodeExc $ (\s -> (prioScaling sc, s)) <$> (sc2Rt sc)
  putStrLn $ systemPrinter sc r
  freshABBsEliminated s r
  --checkIfDeterministic r
  pure (s, r)
collectInputs args = fail $ "wrong number of args: " ++ show args

process :: Exportable t => String -> t -> IO ()
process targetPath tv = do
  putStrLn "generating NuSMV"
  let tp = targetPath ++ ".smv"
  writeFile tp $ unlines $ exportNuSMV tv
  putStrLn $ "written to " ++ tp
  putStrLn "generating COOL model"
  let tp = targetPath ++ ".model"
  writeFile tp $ unlines $ exportCOOLModel tv
  putStrLn $ "written to " ++ tp
  putStrLn "generating COOL spec"
  let tp = targetPath ++ ".spec"
  writeFile tp $ unlines $ exportCOOLSpec tv
  putStrLn $ "written to " ++ tp
main :: IO ()
main = do
  args <- getArgs
  tv <- collectInputs (take 2 args)
  process (fromMaybe "out" $ args ^? ix 2) tv
