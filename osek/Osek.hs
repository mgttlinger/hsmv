{-# LANGUAGE DataKinds #-}
{-# LANGUAGE ImplicitParams #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell #-}
module Osek where

import Control.Lens
import qualified Data.Map as M
import qualified Data.List.NonEmpty as Nel
import Data.Graph.Inductive.Graph
import Data.Graph.Inductive.PatriciaTree
import Data.Maybe
import Data.List
import qualified Data.Set as S
import Data.Either.Utils
import Debug.Trace
import Nusmv

data Conformance = BCC1 | BCC2 | ECC1 | ECC2 deriving (Eq, Show)

data Syscall = StartOS
             | ShutdownOS
  --Resource
             | GetResource String
             | ReleaseResource String
  --Event
             | WaitEvent (S.Set String)
             | ClearEvent (S.Set String)
             | SetEvent (S.Set String)
             | GetEvent (S.Set String)
  --Runnable
             | ActivateTask String
             | ChainTask String
             | TerminateTask
             | Schedule
             | Interrupt Int
             | Iret
             | Idle
  --IR-Management
             | DisableIR
             | EnableIR
             | SuspendIR
             | ResumeIR
             | SuspendOSIR
             | ResumeOSIR
    deriving (Eq)

instance Show Syscall where
  show (ActivateTask n) = "ActivateTask_" ++ n
  show StartOS = "Start"
  show (ReleaseResource n) = "Release_" ++ n
  show (WaitEvent n) = "WaitEvent_" ++ (intercalate "_" $ S.toList n)
  show (ChainTask n) = "ChainTask_" ++ n
  show TerminateTask = "TerminateTask"
  show Schedule = "Schedule"
  show (ClearEvent n) = "ClearEvent_" ++ (intercalate "_" $ S.toList n)
  show (SetEvent n) = "SetEvent_" ++ (intercalate "_" $ S.toList n)
  show DisableIR = "DisableAllInterrupts"
  show EnableIR = "EnableAllInterrupts"
  show SuspendIR = "SuspendAllInterrupts"
  show ResumeIR = "ResumeAllInterrupts"
  show SuspendOSIR = "SuspendOSInterrupts"
  show ResumeOSIR = "ResumeOSInterrupts"
  show (Interrupt k) = "interrupt_" ++ show k
  show Iret = "iret"
  show Idle = "idle"
  show ShutdownOS = "Shutdown"
  show (GetResource x) = "GetResource_" ++ x
  show (GetEvent x) = "GetEvent_" ++ (intercalate "_" $ S.toList x)

artificial :: Syscall -> Bool
artificial (Interrupt _) = True
artificial Iret = True
artificial Idle = True
artificial _ = False

causesSchedule :: Syscall -> Bool
causesSchedule StartOS = True
causesSchedule (ReleaseResource _) = True
causesSchedule (WaitEvent _) = True
causesSchedule (SetEvent _) = True
causesSchedule (ActivateTask _) = True
causesSchedule (ChainTask _) = True
causesSchedule TerminateTask = True
causesSchedule Schedule = True
causesSchedule (Interrupt _) = True
causesSchedule Iret = True
causesSchedule Idle = True
causesSchedule _ = False

isWaitCall :: Syscall -> Bool
isWaitCall (WaitEvent _) = True
isWaitCall _ = False

talksAboutEvent :: Event -> Syscall -> Bool
talksAboutEvent e (WaitEvent evs) = (_ename e) `elem` evs
talksAboutEvent e (ClearEvent evs) = (_ename e) `elem` evs
talksAboutEvent e (SetEvent evs) = (_ename e) `elem` evs
talksAboutEvent e (GetEvent evs) = (_ename e) `elem` evs
talksAboutEvent _ _ = False

data TaskState = Running | Waiting | Ready | Suspended deriving (Eq, Show, Enum)

taskStateValues :: Nel.NonEmpty TaskState
taskStateValues = Running Nel.:| [Waiting, Ready, Suspended]

data InterruptsState = Enabled | Disabled | Cat2Disabled deriving (Eq, Show, Enum)
interruptsStateValues :: Nel.NonEmpty InterruptsState
interruptsStateValues = Enabled Nel.:| [Disabled, Cat2Disabled]

data Event = Event { _ename :: String } deriving (Eq, Ord, Show)

data Resource = Resource { _rname :: String
                         , _internal :: Bool
                         } deriving (Show)
instance Eq Resource where a == b = _rname a == _rname b

data Task = Task { _tname :: String
                 , _basic :: Bool
                 , _tprio :: Int
                 , _preemptable :: Bool
                 , _autostart :: Bool
                 , _internalRes :: Maybe String
                 , _tresources :: [Resource]
                 , _tevents :: [Event]
                 } deriving (Show)
instance Eq Task where a == b = _tname a == _tname b
makeLenses ''Task
                       
data ISR1 = ISR1 { _i1name :: String
                 , _i1prio :: Int
                 , _i1device :: Int
                 } deriving (Show)
instance Eq ISR1 where a == b = _i1name a == _i1name b
makeLenses ''ISR1
  
data ISR2 = ISR2 { _i2name :: String
                 , _i2prio :: Int
                 , _iresources :: [String]
                 , _i2device :: Int
                 } deriving (Show)
instance Eq ISR2 where a == b = _i2name a == _i2name b
makeLenses ''ISR2
                       
data Runnable = ET Task | EZ ISR1 | EO ISR2 deriving (Eq)

class Named t where objName :: t -> String

instance Named Task where
  objName t = "task_" ++ _tname t
instance Named ISR1 where 
  objName i = "isr1_" ++ _i1name i
instance Named ISR2 where 
  objName i = "isr2_" ++ _i2name i
instance Named Resource where
  objName r = "resource_" ++ _rname r
instance Named Event where
  objName e = "event_" ++ _ename e
instance Named Runnable where 
  objName (ET x) = objName x
  objName (EZ x) = objName x
  objName (EO x) = objName x
  
runame :: Runnable -> String
runame (ET x) = _tname x
runame (EZ x) = _i1name x
runame (EO x) = _i2name x

ruprio :: Runnable -> Int
ruprio (ET x) = _tprio x
ruprio (EZ x) = _i1prio x
ruprio (EO x) = _i2prio x

_isISR :: Runnable -> Bool
_isISR (ET _) = False
_isISR _ = True

class Prio t where prio :: t -> Int
instance Prio Runnable where 
  prio (ET x) = prio x
  prio (EZ x) = prio x
  prio (EO x) = prio x
instance Prio Task where prio = _tprio
instance Prio ISR1 where prio = _i1prio
instance Prio ISR2 where prio = _i2prio



data RunnableState = RunnableState { _state :: TaskState
                                   , _nextBlock :: String
                                   , _eventsSet :: [Event]
                                   , _eventsWaiting :: [Event]
                                   , _resourcesOccupied :: [Resource]
                                   }
makeLenses ''RunnableState                     
                    
data OsekState = OsekState { _abb :: String
                           , _running :: Runnable
                           , _runnableStates :: M.Map String RunnableState
                           , _irqState :: InterruptsState
                           }
instance Eq OsekState where a == b = _abb a == _abb b
  
data StaticConf = StaticConf { _confClass :: Conformance
                             , _resources :: [Resource]
                             , _events :: [Event]
                             , _tasks :: [Task]
                             , _isr1s :: [ISR1]
                             , _isr2s :: [ISR2]
                             } deriving (Show)
makeLenses ''StaticConf

data Runtime = Runtime { _start :: OsekState
                       , _graph :: Gr OsekState Syscall
                       }
  
idleTask :: Int -> Task
idleTask p = Task "Idle" True p True True Nothing [] []
scheduler :: Resource 
scheduler = Resource "RES_SCHEDULER" False
preemptable_scheduler :: Resource
preemptable_scheduler = Resource "INT_RES_SCHEDULER" True

edgeToTransition :: Gr OsekState Syscall -> LEdge Syscall -> Maybe (OsekState, Syscall, OsekState)
edgeToTransition cfg (startID, endID, syscall) = do
                                                    start <- lab cfg startID
                                                    end <- lab cfg endID
                                                    pure (start, syscall, end)


stateTransitions :: Gr OsekState Syscall -> [(OsekState, Syscall, OsekState)]
stateTransitions gr = catMaybes $ edgeToTransition (gr) <$> (labEdges gr)
  
states :: Runtime -> Nel.NonEmpty OsekState
states os = Nel.nub $ (_start os) Nel.:| (snd <$> (labNodes $ _graph $ os))
  
occurringSyscalls :: [LEdge Syscall] -> [Syscall]
occurringSyscalls ledgs = (view _3) <$> ledgs

mnn :: Ord t => Nel.NonEmpty t -> t
mnn = Nel.head . Nel.sort
mxn :: Ord t => Nel.NonEmpty t -> t
mxn = Nel.last . Nel.sort
mx :: Ord t => [t] -> Maybe t
mx = listToMaybe . reverse . sort
mn :: Ord t => [t] -> Maybe t
mn = listToMaybe . sort
mmin :: Ord t => t -> Maybe t -> t
mmin a Nothing = a
mmin a (Just b) = min a b
mmax :: Ord t => t -> Maybe t -> t
mmax a Nothing = a
mmax a (Just b) = max a b
maxOr :: Ord t => t -> [t] -> t
maxOr v = fromMaybe v . mx
minOr :: Ord t => t -> [t] -> t
minOr v = fromMaybe v . mn 


runnables :: StaticConf -> Nel.NonEmpty Runnable
runnables sc = Nel.fromList $ (ET <$> _tasks sc) ++ (EZ <$> _isr1s sc) ++ (EO <$> _isr2s sc)
interrupts :: StaticConf ->  [Runnable]
interrupts sc = (EZ <$> _isr1s sc) ++ (EO <$> _isr2s sc)
  
taskPrios :: StaticConf -> [Int]
taskPrios sc = prio <$> _tasks sc
isr1Prios :: StaticConf -> [Int]
isr1Prios sc = prio <$> _isr1s sc
isr2Prios :: StaticConf -> [Int]
isr2Prios sc = prio <$> _isr2s sc
runnablePrios :: StaticConf -> [Int]
runnablePrios sc = taskPrios sc ++ isr1Prios sc ++ isr2Prios sc

tasksUsingResource :: (?sc :: StaticConf) => Resource -> [Task]
tasksUsingResource x = if (x == scheduler) || (x == preemptable_scheduler) then _tasks ?sc else filter (elem x . _tresources) $ _tasks ?sc 

tasksHavingIntResource :: (?sc :: StaticConf) => Resource -> [Task]
tasksHavingIntResource x = (filter (((pure $ _rname x) ==) . _internalRes) $ _tasks ?sc) ++ if x == preemptable_scheduler then filter (not . _preemptable) (_tasks ?sc) else []
  
eventOwner :: (?sc :: StaticConf) => Event -> Task
eventOwner ev = case find (elem ev . _tevents) $ _tasks ?sc of Just t -> t

resourcePrio :: (?sc :: StaticConf) => Resource -> Int
resourcePrio res = 1 + (maxOr 0 $ prio <$> tasksUsingResource res)
    
resourcePrios :: (?sc :: StaticConf) => [Int]
resourcePrios = resourcePrio <$> (_resources ?sc)


minCeilingPrio :: (?sc :: StaticConf) => Int
minCeilingPrio = mmin (mnn (prio <$> runnables ?sc)) (mn resourcePrios)
maxCeilingPrio :: (?sc :: StaticConf) => Int
maxCeilingPrio = mmax (mxn (prio <$> runnables ?sc)) (mx resourcePrios)


subsets :: Ord t => [t] -> [S.Set t]
subsets [] = [S.empty]
subsets (x : xs) = (subsets xs) ++ ((S.insert x) <$> (subsets xs))

nonEmptySubsets :: Ord t => [t] -> [S.Set t]
nonEmptySubsets = filter (not . S.null) . subsets

setEventCalls :: (?sc :: StaticConf) => [Syscall]
setEventCalls = SetEvent <$> (nonEmptySubsets $ _ename <$> (_events ?sc))

clearEventCalls :: (?sc :: StaticConf) => [Syscall]
clearEventCalls = ClearEvent <$> (nonEmptySubsets $ _ename <$> (_events ?sc))

chainCalls :: (?sc :: StaticConf) => [Syscall]
chainCalls = ChainTask . _tname <$> _tasks ?sc

syscalls :: (?sc :: StaticConf) => Nel.NonEmpty Syscall
syscalls = let evs = _ename <$> _events ?sc
               ts = _tname <$> _tasks ?sc
               is = (_i1device <$> _isr1s ?sc) ++ (_i2device <$> _isr2s ?sc)
               res = _rname <$> (filter (not . _internal) $ _resources ?sc)
               calls = nub ([StartOS, ShutdownOS, Schedule, DisableIR, EnableIR, SuspendIR, ResumeIR, SuspendOSIR, ResumeOSIR, Idle, TerminateTask, Iret] ++ ([WaitEvent, SetEvent, ClearEvent, GetEvent] <*> nonEmptySubsets evs) ++ ([ActivateTask, ChainTask] <*> ts) ++ (Interrupt <$> is) ++ ([GetResource, ReleaseResource] <*> res)) in
                  head calls Nel.:| tail calls
                 
schedulingCalls :: (?sc :: StaticConf) => [Syscall]
schedulingCalls = filter causesSchedule $ Nel.toList syscalls


terminatingCalls :: (?sc :: StaticConf) => Runnable -> [Syscall]
terminatingCalls ru@(ET x) = [TerminateTask] ++ [ChainTask $ runame run | run <- Nel.toList $ runnables ?sc, run /= ru, not $ _isISR run]
terminatingCalls ru = [Iret]

activatingCalls :: Runnable -> [Syscall]
activatingCalls ru@(ET x) = [ActivateTask $ runame ru, ChainTask $ runame ru]
activatingCalls (EZ x) = [Interrupt $ _i1device x]
activatingCalls (EO x) = [Interrupt $ _i2device x]
              

canPreempt :: Runnable -> Runnable -> Bool
canPreempt (EZ _) _ = True
canPreempt (EO _) _ = True
canPreempt _ (ET x) = _preemptable x
canPreempt _ _ = False

ceilPrioAt :: (?sc :: StaticConf) => Runnable -> OsekState -> Int
ceilPrioAt ru st = fromMaybe (prio ru) $ do
  rs <- M.lookup (runame ru) $ _runnableStates st
  mx $ resourcePrio <$> _resourcesOccupied rs
  

eventIs :: Event -> OsekState -> Bool
eventIs ev st = any ((elem ev) . _eventsSet) $ _runnableStates st

resourceIs :: Resource -> OsekState -> Bool
resourceIs res st = any ((elem res) . _resourcesOccupied) $ _runnableStates st
  

stateAt :: Runnable -> OsekState -> TaskState 
stateAt ru st = case _state <$> (M.lookup (runame ru) $ _runnableStates st) of
                  Just rs -> rs
                  Nothing -> trace ("invalid runnable: " ++ runame ru) Suspended
                    
abbAt :: Runnable -> OsekState -> String
abbAt ru st = case _nextBlock <$> (M.lookup (runame ru) $ _runnableStates st) of
                Just rs -> rs
                Nothing -> trace ("invalid runnable: " ++ runame ru) ""

runnableABBs :: (?sc :: StaticConf) => (?os :: Runtime) => Nel.NonEmpty String
runnableABBs = let stts = states ?os
                   rus  = runnables ?sc
               in
                 Nel.nub $ abbAt <$> rus <*> stts

waitStateAt :: Task -> OsekState -> [Bool]
waitStateAt t st = (\e -> elem e $ (_eventsWaiting $ fromJust $ M.lookup (_tname t) $ _runnableStates st)) <$> _tevents t


instance Literable Syscall NEnum where
  lit = lit . show

instance Literable TaskState NEnum where
  lit = lit . show
  
instance Literable InterruptsState NEnum where
  lit = lit . show
