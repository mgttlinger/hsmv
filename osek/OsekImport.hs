{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}

module OsekImport where

import Data.Aeson hiding ((<?>))
import qualified Data.Aeson.Types as A
import Osek
import Text.Parsec
import Text.Parsec.Text
import Text.Parsec.Char
import Text.Parsec.Combinator
import Data.Graph.Inductive.Graph
import Data.Graph.Inductive.PatriciaTree
import Data.List
import Data.Maybe
import Control.Monad
import qualified Data.Text as T
import Control.Lens
import Data.Text.Encoding
import qualified Data.Map as M
import qualified Data.Set as S
import Data.Scientific

newtype ABB = ABB { val :: String }
newtype OsekRefEdge = OsekRefEdge { ref :: (ABB, ABB, Syscall) }

runnable :: StaticConf -> String -> Either String Runnable
runnable sc nme = case find ((nme ==) . runame) $ runnables sc of
                    Just res -> pure res
                    Nothing -> Left $ nme ++ " is not a known runnable"
event :: StaticConf -> String -> Either String Event
event sc nme = case find ((nme ==) . _ename) $ _events sc of
                 Just res -> pure res
                 Nothing -> Left $ nme ++ " is not a known event"
                 
resource :: StaticConf -> String -> Either String Resource
resource sc nme = case find ((nme ==) . _rname) $ _resources sc of
                    Just res -> pure res
                    Nothing -> Left $ nme ++ " is not a known resource"

instance FromJSON Conformance where 
  parseJSON (String "BCC1") = pure BCC1
  parseJSON (String "BCC2") = pure BCC2
  parseJSON (String "ECC1") = pure ECC1
  parseJSON (String "ECC2") = pure ECC2

instance FromJSON Resource where
  parseJSON = withObject "Resource" $ \v -> Resource
                                            <$> v .: "name"
                                            <*> v .: "internal"

instance FromJSON Event where 
  parseJSON = withObject "Event" $ \v -> Event <$> v .: "name"

instance FromJSON TaskState where
  parseJSON (String "suspended") = pure Suspended
  parseJSON (String "ready") = pure Ready
  parseJSON (String "waiting") = pure Waiting
  parseJSON (String "running") = pure Running

sanitizeNext :: String -> String
sanitizeNext s = filter ('/' /=) s

instance FromJSON (StaticConf -> Either String RunnableState) where 
  parseJSON = withObject "RunnableState generator" $ \ v -> do
    stte <- v .: "state"
    nxt <- v .: "next-block"
    eventsS <- v .: "events-set"
    eventsW <- v .: "events-waiting"
    resourcesTaken <- v .: "resources-taken"
    pure $ \sc -> (RunnableState stte $ fromMaybe "fresh" $ sanitizeNext <$> nxt) <$> (traverse (event sc) eventsS) <*> (traverse (event sc) eventsW) <*> (traverse (resource sc) resourcesTaken)

toISState :: Bool -> Bool -> InterruptsState
toISState False False = Enabled
toISState False True  = Cat2Disabled
toISState True  _     = Disabled


instance FromJSON ABB where
  parseJSON = withScientific "ABB" $ \ s -> pure $ sanitizeABB s
    where
      repl '.' = '_'
      repl c = c
      sanitizeABB :: Scientific -> ABB
      sanitizeABB s = ABB $ "ABB_" ++ (repl <$> show s)

instance FromJSON (StaticConf -> Either String OsekState) where 
  parseJSON = withObject "OsekState generator" $ \ v -> do
    abb <- v .: "id"
    running <- v .: "runnable"
    taskDict <- v .: "subtasks"
    irqstate <- toISState <$> (v .: "irq-block-all") <*> (v .: "irq-block-os")
    pure $ \sc -> OsekState (val abb) <$> (runnable sc $ fromMaybe "Idle" running) <*> (correctState running <$> traverse (\s -> s sc) taskDict) <*> (pure irqstate)
    where
      correctState :: Maybe String -> M.Map String RunnableState -> M.Map String RunnableState
      correctState Nothing m = M.adjust (set state Running) "Idle" m
      correctState (Just ru) m = M.adjust (set state Running) ru m




instance FromJSON Syscall where
  parseJSON = withText "Syscall" $ \s -> toResult $ parse syscallP "" s
    where
      toResult :: Either ParseError a -> A.Parser a
      toResult (Left txt) = fail $ show txt
      toResult (Right av) = pure av
      angles = between (char '<') (char '>')
      token = string
      parens = between (char '(') (char ')')
      brackets = between (char '[') (char ']')
      subtaskP :: Parser String
      subtaskP = (angles $ (token "Subtask " *> (many1 alphaNum) <* (optional $ token " ISR"))) <?> "Subtask"
      eventP :: Parser String
      eventP = (angles $ (token "Event " *> (do
                             a <- (many1 alphaNum) <* string "__"
                             b <- (many1 alphaNum) <* token " task:" <* subtaskP
                             pure $ a ++ "__" ++ b))) <?> "Event"
      eventsP :: Parser (S.Set String)
      eventsP = parens $ brackets $ S.fromList <$> sepBy1 eventP (string ", ")
      resourceP :: Parser String
      resourceP = (angles $ (token "Res " *> many1 (try alphaNum <|> try (char '_')) <* token " pri:" <* many1 alphaNum)) <?> "Resource"
      setEventP :: Parser Syscall
      setEventP = (SetEvent <$> (string "SetEvent" *> eventsP)) <?> "SetEvent"
      clearEventP :: Parser Syscall
      clearEventP = (ClearEvent <$> (string "ClearEvent" *> eventsP)) <?> "ClearEvent"
      activateTaskP :: Parser Syscall
      activateTaskP = (ActivateTask <$> (string "ActivateTask" *> (parens $ subtaskP))) <?> "ActivateTask"
      terminateTaskP :: Parser Syscall
      terminateTaskP = string "TerminateTask()" *> pure TerminateTask <?> "Terminate"
      chainTaskP :: Parser Syscall
      chainTaskP = (ChainTask <$> (string "ChainTask" *> parens subtaskP)) <?> "Chain"
      scheduleP :: Parser Syscall
      scheduleP = (string "Schedule()" *> pure Schedule) <?> "Schedule"
      getResP :: Parser Syscall
      getResP = (GetResource <$> (string "GetResource" *> parens resourceP)) <?> "GetResource"
      releaseResP :: Parser Syscall
      releaseResP = (ReleaseResource <$> (string "ReleaseResource" *> parens resourceP)) <?> "ReleaseResource"
      waitEventP :: Parser Syscall
      waitEventP = (WaitEvent <$> (string "WaitEvent" *> eventsP)) <?> "WaitEvent"
      startOSP :: Parser Syscall
      startOSP = (string "StartOS()" *> pure StartOS) <?> "StartOS"
      interruptP :: Parser Syscall
      interruptP = (Interrupt <$> (string "trigger_interrupt_" *> (read <$> many1 digit) <* string "()")) <?> "Interrupt"
      iretP :: Parser Syscall
      iretP = (pure Iret <* string "iret" <* (parens $ subtaskP)) <?> "iret"
      idleP :: Parser Syscall
      idleP = (pure Idle <* string "Idle()") <?> "idle"
      resumeAllInterruptsP :: Parser Syscall
      resumeAllInterruptsP = (pure ResumeIR <* string "ResumeAllInterrupts()") <?> "ResumeAllInterrupts"
      suspendAllInterruptsP :: Parser Syscall
      suspendAllInterruptsP = (pure SuspendIR <* string "SuspendAllInterrupts()") <?> "SuspendAllInterrupts"
      disableAllInterruptsP :: Parser Syscall
      disableAllInterruptsP = (pure DisableIR <* string "DisableAllInterrupts()") <?> "DisableAllInterrupts"
      enableAllInterruptsP :: Parser Syscall
      enableAllInterruptsP = (pure EnableIR <* string "EnableAllInterrupts()") <?> "EnableAllInterrupts"
      syscallP :: Parser Syscall
      syscallP = string "ABB" *> ((many1 digit) *> string "/" *> (try setEventP <|> try waitEventP <|> try clearEventP <|> try activateTaskP <|> try terminateTaskP <|> try chainTaskP <|> try scheduleP <|> try getResP <|> try releaseResP <|> try interruptP <|> try iretP <|> try idleP <|> try startOSP <|> try resumeAllInterruptsP <|> try suspendAllInterruptsP <|> try disableAllInterruptsP <|> try enableAllInterruptsP)) <?> "Syscall"
    
instance FromJSON OsekRefEdge where
  parseJSON = withObject "ref Edge" $ \ v -> do
    from <- v .: "from"
    to <- v .: "to"
    call <- v .: "label"
    pure $ OsekRefEdge (from, to, call)

     

enrich' :: [OsekState] -> ABB -> Either String OsekState
enrich' states name = case find ((val name ==) . _abb) states of
                        Just res -> pure res
                        Nothing -> Left $ "state " ++ val name ++ " missing from known states"

enrich :: [OsekState] -> OsekRefEdge -> Either String (OsekState, OsekState, Syscall)
enrich states (OsekRefEdge (from, to, msc)) = pair3 <$> (enrich' states from) <*> (enrich' states to) <*> (pure msc)
  where 
    pair3 :: a -> b -> c -> (a, b, c)
    pair3 av bv cv = (av, bv, cv)


buildG :: [OsekState] -> [(OsekState, OsekState, Syscall)] -> Gr OsekState Syscall
buildG nodeLs edgeLs = let gra = insNodes lnodes empty in
                         insEdges (toLEdge <$> edgeLs) gra
                         where
                           lnodes :: [LNode OsekState]
                           lnodes = zip (newNodes (length nodeLs) (empty :: Gr OsekState Syscall)) nodeLs
                           cvt :: OsekState -> Node
                           cvt s = fromJust $ fst <$> find ((s ==) . snd) lnodes
                           toLEdge :: (OsekState, OsekState, Syscall) -> LEdge Syscall
                           toLEdge (f, t, s) = (cvt f, cvt t, s)
                           

instance FromJSON (StaticConf -> Either String Runtime) where
  parseJSON = withObject "Runtime generator" $ \ v -> do
    nodes <- v .: "nodes"
    edgs <-  v .: "edges"
    start <- v .: "entry"
    pure $ \sc -> let stts = traverse (fap sc) nodes
                      graphOrError = (buildG <$> stts <*> join (traverse <$> (enrich <$> stts) <*> (pure edgs)))
                      oldStart = (join (enrich' <$> stts <*> (pure start))) 
                  in
                    do
                      graph <- graphOrError
                      newStart <- (findStart =<< graphOrError)
                      s <- nodeLookup graph =<< oldStart
                      checkForErrors s graph
                      filteredGraph <- (labfilter <$> ((/=) <$> oldStart) <*> graphOrError)
                      pure $ Runtime newStart filteredGraph
    where 
      fap :: v -> (v -> u) -> u
      fap v f = f v
      nodeLookup ::  Gr OsekState Syscall -> OsekState -> Either String Node
      nodeLookup graph el = maybe (Left "graph doesn't contain start node") Right $ view _1 <$> (find ((el ==) . (view _2)) $ labNodes graph)
      checkForErrors :: Node -> Gr OsekState Syscall -> Either String ()
      checkForErrors start graph = if (deg graph start) /= 1 then Left "The start node has to have a degree of one!" else pure ()
      findStart :: Gr OsekState Syscall -> Either String OsekState
      findStart graph = maybe (Left "system has only start state") (Right) $ (find ((StartOS ==) . view _3) $ labEdges graph) >>= lab graph . view _2 

instance FromJSON ISR1 where 
  parseJSON = withObject "ISR1" $ \v -> ISR1
                                        <$> v .: "name"
                                        <*> v .: "priority"
                                        <*> v .: "device"

instance FromJSON ISR2 where 
  parseJSON = withObject "ISR2" $ \v -> ISR2
                                        <$> v .: "name" 
                                        <*> v .: "priority"
                                        <*> v .: "resources"
                                        <*> v .: "device"

instance FromJSON ([Event] -> [Resource] -> A.Parser Task) where 
  parseJSON = withObject "Task" $ \v -> do 
    name <- v .: "name" 
    basic <- v .: "basic" 
    prio <- v .: "priority" 
    preemp <- v .: "preemptable" 
    autostart <- v .: "autostart"
    internal <- pure Nothing --internal res
    res <- v .: "resources" 
    evs <- v .: "events" 
    pure $ \es -> \rs -> Task name basic prio preemp autostart internal <$> (traverse (resLU rs) res) <*> (traverse (evsLU es) evs)
    where
      liftMaybe :: Maybe t -> A.Parser t
      liftMaybe (Just t) = pure t
      liftMaybe Nothing = fail "invalid context lookup"
      resLU :: [Resource] -> String -> A.Parser Resource
      resLU rs n = liftMaybe $ find ((n ==) . _rname) rs
      evsLU :: [Event] -> String -> A.Parser Event
      evsLU es n = liftMaybe $ find ((n ==) . _ename) es


    
instance FromJSON StaticConf where
  parseJSON = withObject "StaticConf" $ \ v -> do
    conf <- pure ECC1
    resources <- (++[scheduler, preemptable_scheduler]) <$> (v .: "resources")
    events <- v .: "events"
    tasks <- join $ (traverse (\f -> f events resources)) <$> (v .: "tasks")
    isr1s <- v .: "isr1s"
    isr2s <- v .: "isr2s"
    pure $ StaticConf conf resources events ((idleTask $ (\s -> s - 1) $ minOr 1 $ _tprio <$> tasks) : tasks) isr1s isr2s
    

    

