{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ImplicitParams #-}
module OsekExport where

import COOL
import Nusmv
import Osek hiding (prio)
import Export
import Data.Graph.Inductive
import Data.Map.Strict (Map, insertWith, insert)
import Data.List hiding (insert)
import Data.Maybe
import qualified Data.List.NonEmpty as Nel
import Control.Arrow
import Control.Lens
import Control.Parallel.Strategies

data ReadyReason = Activate
                 | SelfChain
                 | Preempted
                 | EventSet
                 deriving (Eq, Show, Enum)
instance Literable ReadyReason NEnum where lit = lit . show
readyReasons :: Nel.NonEmpty ReadyReason
readyReasons = Activate Nel.:| [SelfChain, Preempted, EventSet]

--Event
g_eventState :: (?sc :: StaticConf) => (?os :: Runtime) => Event -> StateVar NBool
g_eventState ev = StateVar (CVar ("state_" ++ objName ev) BoolRange) (lit $ eventIs ev $ _start ?os) (g_fromStateWD (eventIs ev))
    
e_setCall :: (?sc :: StaticConf) => MemberDef Event (NSet NEnum)
e_setCall = MemberDef "setCall" (\ e -> lit <$> (filter (talksAboutEvent e) setEventCalls))

e_clearCall :: (?sc :: StaticConf) => MemberDef Event (NSet NEnum)
e_clearCall = MemberDef "clearCall" (\ e -> lit <$> (filter (talksAboutEvent e) clearEventCalls))

e_eventState :: (?sc :: StaticConf) => (?os :: Runtime) => MemberDef Event NBool
e_eventState = AutoInput "isSet" g_eventState

e_ownerState :: (?sc :: StaticConf) => (?os :: Runtime) => MemberDef Event NEnum
e_ownerState = DirectInput "ownerState" $ \ e -> rstate $ ET $ eventOwner e
  
e_eventStateBehavesCorrectly :: (?sc :: StaticConf) => (?os :: Runtime) => [Spec]
e_eventStateBehavesCorrectly = CTLSpec Nothing . AG <$>
  [ (e_ownerState `is` Suspended) ===> AX ((((e_ownerState `is` Ready) \/ (e_ownerState `is` Running))) ===> neg set)
  , ((e_ownerState `is` Running) /\ set) ===> AX (neg set <==> clearCalled)
  , (neg set) ===> AX (setCalled <==> set)
  ]
  where
    set :: Embeddable (NExpr NBool) t => t
    set = embed $ ref e_eventState
    setCalled :: Embeddable (NExpr NBool) t => t
    setCalled = syscallInput `ins` ref e_setCall
    clearCalled :: Embeddable (NExpr NBool) t => t
    clearCalled = syscallInput `ins` ref e_clearCall
  
eventModule :: (?sc :: StaticConf) => (?os :: Runtime) => Module Event
eventModule = add e_ownerState $ add syscallInput $ add e_setCall $ add e_clearCall $ add e_eventState $ specModule "Event" (objName) e_eventStateBehavesCorrectly
--Properties
eventSet :: (?sc :: StaticConf) => (?os :: Runtime) => Property Event NBool
eventSet ev = refIn [inst eventModule ev] e_eventState
--END Event

--Resource
g_resourceState :: (?sc :: StaticConf) => (?os :: Runtime) => Resource -> StateVar NBool
g_resourceState res = StateVar (CVar ("state_" ++ objName res) BoolRange) (lit $ resourceIs res $ _start ?os) (g_fromStateWD (resourceIs res))
    
r_resourceState :: (?sc :: StaticConf) => (?os :: Runtime) =>  MemberDef Resource NBool
r_resourceState = AutoInput "isOccupied" g_resourceState

r_resourcePrioDef :: (?sc :: StaticConf) => MemberDef Resource NInt
r_resourcePrioDef = MemberDef "priority" resourcePrio

resourceModule :: (?sc :: StaticConf) => (?os :: Runtime) => Module Resource
resourceModule = add r_resourceState $ add r_resourcePrioDef $ emptyModule "Resource" (objName)
--Properties
resPrio :: (?sc :: StaticConf) => (?os :: Runtime) => Property Resource NInt
resPrio res = refIn [inst resourceModule res] r_resourcePrioDef
    
resOccupied :: (?sc :: StaticConf) => (?os :: Runtime) => Property Resource NBool
resOccupied res = refIn [inst resourceModule res] r_resourceState
--END Resource

-- Global
g_systemState :: (?sc :: StaticConf) => (?os :: Runtime) => StateVar NEnum
g_systemState = StateVar (CVar "state" $ enumRange $ Nel.nub $ _abb <$> states ?os) (lit $ _abb $ _start ?os) (\ v -> NDCaseExpr $ transitionHelper v <$> (groupBy (\ a b -> view _1 a == view _1 b) $ stateTransitions $ _graph ?os))
  where
    transitionHelper :: Variable NEnum -> [(OsekState, Syscall, OsekState)] -> (NExpr NBool, NExpr (NSet NEnum))
    transitionHelper v ((from, _, to) : tl) = (v `is` _abb from, SetExpr $ lit . _abb <$> (to : (view _3 <$> tl)))
    
state :: (?sc :: StaticConf) => (?os :: Runtime) => Property OsekState NBool
state s = g_systemState `is` _abb s
     
g_syscall :: (?sc :: StaticConf) => (?os :: Runtime) => StateVar NEnum
g_syscall = StateVar (CVar "syscall" $ enumRange $ nshow . lit <$> syscalls) (lit StartOS) (\ v -> CaseExpr $ (helper <$> (stateTransitions $ _graph ?os)) ++ [defaultFCase v])
  where
    helper :: (OsekState, Syscall, OsekState) -> (NExpr NBool, NExpr NEnum)
    helper (from, sys, to) = (OsekExport.state from /\ (NextExpr $ OsekExport.state to), lit sys)
    

g_fromStateWD :: (Ord (NExpr t), Literable l t) => (?sc :: StaticConf) => (?os :: Runtime) => (OsekState -> l) -> Variable t -> NExpr t
g_fromStateWD f v = mce (withStrategy (parList rpar) $ mergeSimilar $ stateToCasePair (lit . f) <$> (states ?os)) [defaultFCase v]
  where
    mce :: [(NExpr NBool, NExpr t)] -> [(NExpr NBool, NExpr t)] -> NExpr t
    mce [a] _ = snd a
    mce as d = CaseExpr $ as ++ d
    stateToCasePair :: (OsekState -> NExpr t) -> OsekState -> (NExpr NBool, NExpr t)
    stateToCasePair f s = ((NextExpr $ OsekExport.state s), f s)
    mergeFormula :: NExpr NBool -> NExpr NBool -> NExpr NBool
    mergeFormula (NextExpr a) (NextExpr b) = NextExpr $ mergeFormula a b
    mergeFormula a b = a \/ b
    mergePairs :: Nel.NonEmpty (NExpr NBool, u) -> (NExpr NBool, u)
    mergePairs ps = (foldr mergeFormula (embed False) $ fst <$> ps, snd $ Nel.head ps)
    trivial :: [Nel.NonEmpty (NExpr NBool, u)] -> [(NExpr NBool, u)]
    trivial [es] = [(embed True, snd $ Nel.head es)]
    trivial ess = mergePairs <$> ess
    mergeSimilar :: Ord (NExpr t) => Nel.NonEmpty (NExpr NBool, NExpr t) -> [(NExpr NBool, NExpr t)]
    mergeSimilar ps = trivial $ (Nel.groupBy (\ a b -> snd a == snd b) $ Nel.sortBy (\ a b -> compare (snd a) (snd b)) ps)


syscallInput :: (?sc :: StaticConf) => (?os :: Runtime) => OutsideState NEnum
syscallInput = OutsideState "p_syscall" [name $ var g_syscall]
   
ceilingPrioRange :: (?sc :: StaticConf) => Range NInt
ceilingPrioRange = IntRange minCeilingPrio maxCeilingPrio
        
syscallIs :: (?sc :: StaticConf) => (?os :: Runtime) => Syscall -> NExpr NBool
syscallIs call = ref syscallInput === lit call
   
syscallIn :: (?sc :: StaticConf) => (?os :: Runtime) => NExpr (NSet NEnum) -> NExpr NBool
syscallIn s = InSetExpr (ref syscallInput) s

nextSyscall :: (?sc :: StaticConf) => (?os :: Runtime) => Syscall -> NExpr NBool
nextSyscall = NextExpr . syscallIs

g_runningState :: (?sc :: StaticConf) => (?os :: Runtime) => StateVar NEnum
g_runningState = StateVar (CVar "running" $ enumRange $ Nel.nub $ runame <$> runnables ?sc) (lit $ runame . _running $ _start ?os) (g_fromStateWD $ runame . _running)

g_interruptsState :: (?sc :: StaticConf) => (?os :: Runtime) => StateVar NEnum
g_interruptsState = StateVar (CVar "interruptsState" $ enumRange interruptsStateValues) (lit $ _irqState $ _start ?os) (g_fromStateWD _irqState)


--Runnable
g_ceilingPrio :: (?sc :: StaticConf) => (?os :: Runtime) => Runnable -> StateVar NInt
g_ceilingPrio r = StateVar (CVar ("ceil_" ++ objName r) $ ceilingPrioRange) (lit $ ceilPrioAt r $ _start ?os) (g_fromStateWD $ ceilPrioAt r)
    
ru_ceilingPrioDef :: (?sc :: StaticConf) => (?os :: Runtime) => MemberDef Runnable NInt
ru_ceilingPrioDef = AutoInput "ceiling_priority" g_ceilingPrio

t_basicDef :: MemberDef Task NBool
t_basicDef = MemberDef "isBasic" _basic

t_autostartDef :: MemberDef Task NBool
t_autostartDef = MemberDef "autostart" _autostart
  
g_runnableState :: (?sc :: StaticConf) => (?os :: Runtime) => Runnable -> StateVar NEnum
g_runnableState x = StateVar (CVar ("state_" ++ objName x) (enumRange taskStateValues)) (lit $ stateAt x $ _start ?os) (g_fromStateWD $ stateAt x)

ru_stateDef :: (?sc :: StaticConf) => (?os :: Runtime) => MemberDef Runnable NEnum
ru_stateDef = AutoInput "state" g_runnableState
    
g_runnableAbb :: (?sc :: StaticConf) => (?os :: Runtime) => Runnable -> StateVar NEnum
g_runnableAbb x = StateVar (CVar ("abb_" ++ objName x) (enumRange $ Nel.nub $ abbAt x <$> states ?os)) (lit $ abbAt x $ _start ?os) (g_fromStateWD $ abbAt x)
 
ru_abbDef :: (?sc :: StaticConf) => (?os :: Runtime) => MemberDef Runnable NEnum
ru_abbDef = AutoInput "abb" g_runnableAbb

g_fromStateArr :: Literable l t => (?sc :: StaticConf) => (?os :: Runtime) => (OsekState -> [l]) -> Variable (NArray t) -> [NExpr t]
g_fromStateArr f v = do
  result <- zip [0..] $ transpose (((id &&& ((lit <$>). f)) >>> sequence) <$> (Nel.toList $ states ?os))
  pure $ CaseExpr $ pairToCasePairList (ref v) result
  where
    exprMaker :: (OsekState, NExpr t) -> (NExpr NBool, NExpr t)
    exprMaker (stt, l) = (NextExpr $ OsekExport.state stt, l)
    pairToCasePairList :: NExpr (NArray t) -> (Int, [(OsekState, NExpr t)]) -> [(NExpr NBool, NExpr t)]
    pairToCasePairList aref (idx, svs) = (exprMaker <$> svs) ++ [(NextExpr $ embed True, ArrayIndex idx $ aref)]


g_taskEventState :: (?sc :: StaticConf) => (?os :: Runtime) => Task -> ArrayVar NBool
g_taskEventState t = ArrayVar (CVar ("events_" ++ objName t) $ ArrayRange 0 ((length $ _tevents t) - 1) BoolRange) (lit <$> (waitStateAt t $ _start ?os)) (g_fromStateArr (waitStateAt t))

t_eventState :: (?sc :: StaticConf) => (?os :: Runtime) => MemberDef Task (NArray NBool)
t_eventState = AutoInput "waitingFor" g_taskEventState

ru_ABBOnlyChangesWhenRunning :: (?sc :: StaticConf) => (?os :: Runtime) => [Spec]
ru_ABBOnlyChangesWhenRunning = CTLSpec Nothing . AG . (\abb ->
                                     abbIs abb ===> aw (abbIs abb) (ru_stateDef `is` Running)) <$> (Nel.toList runnableABBs)
  where
    abbIs :: Embeddable (NExpr NBool) t => String -> t
    abbIs a = ru_abbDef `is` a

g_highestPrio :: (?sc :: StaticConf) => (?os :: Runtime) => Runnable -> StateVar NBool
g_highestPrio r = StateVar (CVar ("highestPrio_" ++ objName r) BoolRange) (lit $ ihp $ _start ?os) (g_fromStateWD ihp)
  where
    ihp s = ((stateAt r s == Ready || stateAt r s == Running) && all (\ oru -> (not $ ((stateAt oru s == Ready) || (stateAt oru s == Running))) || (((ceilPrioAt r s) >= (ceilPrioAt oru s)))) ((Nel.toList $ runnables ?sc) \\ [r]))

g_highestPrioOld :: (?sc :: StaticConf) => (?os :: Runtime) => Runnable -> Variable NBool
g_highestPrioOld r = Definition ("highestPrio_" ++ objName r) $ (isReady r \/ isRunning r) /\ fAll ((Nel.toList $ runnables ?sc) \\ [r]) (\ oru -> ((isReady oru) \/ (isRunning oru)) ===> ((NGt (prio r) (prio oru)) {-\/ ((NEq (prio r) (prio oru)) /\ (betterReason r oru))-}))
  -- where
  --   -- reason (ET x) rs = (refIn [inst taskModule x] ru_readyReason) === lit rs
  --   -- reason (EZ x) rs = (refIn [inst isr1Module x] ru_readyReason) === lit rs
  --   -- reason (EO x) rs = (refIn [inst isr2Module x] ru_readyReason) === lit rs
  --   betterReason :: Runnable -> Runnable -> NExpr NBool
  --   betterReason a b = (reason a Preempted) \/ (reason b SelfChain) 

ru_isHighestPrioDef :: (?sc :: StaticConf) => (?os :: Runtime) => MemberDef Runnable NBool
ru_isHighestPrioDef = AutoInput "isHighestPrio" g_highestPrio 

-- ru_readyReason :: (?sc :: StaticConf) => (?os :: Runtime) => StateVar NEnum
-- ru_readyReason = StateVar (CVar "readyReason" $ enumRange readyReasons) (lit Activate) (\ v ->
--   CaseExpr [ ((ru_stateDef `is` Suspended) /\ (NextExpr $ ru_stateDef `is` Ready), lit Activate)
--            , ((ru_stateDef `is` Running) /\ (NextExpr $ (syscallInput `ins` (lit $ lit <$> chainCalls)) /\ (ru_stateDef `is` Ready)), lit SelfChain)
--            , ((ru_stateDef `is` Running) /\ (NextExpr $ ru_stateDef `is` Ready), lit Preempted)
--            , ((ru_stateDef `is` Waiting) /\ (NextExpr $ ru_stateDef `is` Ready), lit EventSet)
--            , (lit True, ref v)
--            ])
  
taskModule :: (?sc :: StaticConf) => (?os :: Runtime) => Module Task
taskModule = add (derive ET ru_isHighestPrioDef) $ add t_eventState $ add (derive ET ru_abbDef) $ add (derive ET ru_stateDef) $ add (derive ET ru_ceilingPrioDef) $ add syscallInput $ add t_basicDef $ add t_autostartDef $ 
  specModule "Task" objName ([onlyExtendedWait, autostartRespected] ++ ru_ABBOnlyChangesWhenRunning)
  where
    autostartRespected :: Spec
    autostartRespected = CTLSpec (Just "autostartRespected") $ (embed $ ref t_autostartDef) <==> (ru_stateDef `ins` (SetExpr [lit Ready, lit Running]))
    onlyExtendedWait :: Spec
    onlyExtendedWait = CTLSpec (Just "onlyExtendedWait") $ AG $ (ru_stateDef `is` Waiting) ===> (t_basicDef `is` False)

isr1Module :: (?sc :: StaticConf) => (?os :: Runtime) => Module ISR1
isr1Module = add syscallInput $ add (derive EZ ru_isHighestPrioDef) $ add (derive EZ $ ru_abbDef) $ add (derive EZ $ ru_stateDef) $ add (derive EZ ru_ceilingPrioDef) $ 
  specModule "ISR1" objName ru_ABBOnlyChangesWhenRunning
            
isr2Module :: (?sc :: StaticConf) => (?os :: Runtime) => Module ISR2
isr2Module = add syscallInput $ add (derive EO ru_isHighestPrioDef) $ add (derive EO ru_abbDef) $ add (derive EO ru_stateDef) $ add (derive EO ru_ceilingPrioDef) $ 
  specModule "ISR2" objName ru_ABBOnlyChangesWhenRunning
--Properties 
rstate :: (?sc :: StaticConf) => (?os :: Runtime) => Property Runnable NEnum
rstate (ET x) = refIn [inst taskModule x] ru_stateDef
rstate (EZ x) = refIn [inst isr1Module x] ru_stateDef
rstate (EO x) = refIn [inst isr2Module x] ru_stateDef
  
prio :: (?sc :: StaticConf) => (?os :: Runtime) => Property Runnable NInt
prio (ET x) = refIn [inst taskModule x] ru_ceilingPrioDef
prio (EZ x) = refIn [inst isr1Module x] ru_ceilingPrioDef
prio (EO x) = refIn [inst isr2Module x] ru_ceilingPrioDef

isRunning :: (?sc :: StaticConf) => (?os :: Runtime) => Property Runnable NBool
isRunning r = rstate r === lit Running 
isReady :: (?sc :: StaticConf) => (?os :: Runtime) => Property Runnable NBool
isReady r = rstate r === lit Ready 
isSuspended :: (?sc :: StaticConf) => (?os :: Runtime) => Property Runnable NBool
isSuspended r = rstate r === lit Suspended
isWaiting :: (?sc :: StaticConf) => (?os :: Runtime) => Property Runnable NBool
isWaiting r = rstate r === lit Waiting

isWaitingFor :: (?sc :: StaticConf) => (?os :: Runtime) => Event -> Property Runnable NBool
isWaitingFor e (ET t) = case elemIndex e (_tevents t) of
  Just i  -> ArrayIndex i (refIn [inst taskModule t] t_eventState)
  Nothing -> embed False
isWaitingFor _ _ = embed False

isHighestPrio :: (?sc :: StaticConf) => (?os :: Runtime) => Property Runnable NBool
isHighestPrio (ET x) = refIn [inst taskModule x] ru_isHighestPrioDef
isHighestPrio (EZ x) = refIn [inst isr1Module x] ru_isHighestPrioDef
isHighestPrio (EO x) = refIn [inst isr2Module x] ru_isHighestPrioDef
--END Runnable

g_interruptsStateIsRespected :: (?sc :: StaticConf) => (?os :: Runtime) => Spec
g_interruptsStateIsRespected = CTLSpec (Just "interruptsStateIsRespected") $ AG $
  ((irsIs Disabled) ===> (fAll (interrupts ?sc) notHappening))
  /\ ((irsIs Cat2Disabled) ===> (fAll i2 notHappening))
  where 
    irsIs :: Embeddable (NExpr NBool) t => InterruptsState -> t
    irsIs s = g_interruptsState `is` s 
    i2 :: [Runnable]
    i2 = EO <$> _isr2s ?sc
    notHappening :: Runnable -> CTL
    notHappening x = neg $ (embed $ neg $ isRunning x) /\ (EX $ embed $ isRunning x)

g_interruptsStateTransitions :: (?sc :: StaticConf) => (?os :: Runtime) => Spec
g_interruptsStateTransitions = CTLSpec (Just "interruptsStateTransitions") $ AG $
  irsStays Disabled [EnableIR, ResumeIR, ResumeOSIR]
  /\ irsStays Enabled [DisableIR, SuspendIR, SuspendOSIR]
  /\ irsStays Cat2Disabled [ResumeOSIR]
  where
    irsStays :: InterruptsState -> [Syscall] -> CTL
    irsStays s sys = (g_interruptsState `is` s) ===> aw (g_interruptsState `is` s) (g_syscall `ins` (SetExpr $ lit <$> sys))
    
g_cat1NoAPICalls :: (?sc :: StaticConf) => (?os :: Runtime) => [Spec]
g_cat1NoAPICalls = (\ i -> CTLSpec Nothing $ AG $
                     (embed $ isRunning i) ===> aw (exst allowedCalls (\ sca -> AX $ g_syscall `is` sca)) (embed $ neg $ isRunning i)) . EZ <$> _isr1s ?sc
  where 
    allowedCalls :: [Syscall]
    allowedCalls = [DisableIR, EnableIR, SuspendIR, ResumeIR, SuspendOSIR, ResumeOSIR]

g_cat2CertainApiCalls :: (?sc :: StaticConf) => (?os :: Runtime) => [Spec]
g_cat2CertainApiCalls = (\ i -> CTLSpec Nothing $ AG $
                          (embed $ isRunning i) ===> aw (fAll forbiddenCalls (\ sca -> AX $ neg $ g_syscall `is` sca)) (embed $ neg $ isRunning i)) . EO <$> _isr2s ?sc
  where 
    forbiddenCalls :: [Syscall]
    forbiddenCalls = [TerminateTask, Schedule, StartOS] ++ (ChainTask . _tname <$> (_tasks ?sc)) ++ ([ClearEvent, WaitEvent] <*> (nonEmptySubsets $ _ename <$> _events ?sc))


g_nonPreemptableTasksNotPreempted :: (?sc :: StaticConf) => (?os :: Runtime) => [Spec]
g_nonPreemptableTasksNotPreempted = (\ t -> CTLSpec Nothing $ AG $
                                      preempted t ===> (embed $ _preemptable t)) <$> _tasks ?sc
  where
    preempted :: Task -> CTL
    preempted t = (embed $ isRunning $ ET t) /\ (AX $ embed $ (isReady $ ET t) /\ (exst (_tasks ?sc \\ [t]) (\ ot -> isRunning $ ET ot)))


g_fastStateTransitionsAreValid :: (?sc :: StaticConf) => (?os :: Runtime) => [Spec]
g_fastStateTransitionsAreValid = (CTLSpec Nothing . AG) <$> ((\ r -> 
                                    ((\ b -> (st Suspended r) ===> b) <$>
                                       (((\ ap ac -> ((neg ap) ===> (AX $ ((g_syscall `is` ac) ===> (st Ready r))))) <$> ape r <*> activatingCalls r) ++
                                       ((\ ap -> ((neg ap) ===> (AX $ ((sysOf $ activatingCalls r) <=== (st Ready r))))) <$> ape r) ++
                                       ((\ ac -> AX (st Suspended r ===> (neg $ g_syscall `is` ac))) <$> activatingCalls r) ++
                                       [ ((allOtherPreemptable r) ===> (AX $ (sysOf $ activatingCalls r) ===> ((iconnective r) (st Running r) (embed $ isHighestPrio r))))
                                       , AX (st Suspended r \/ st Ready r \/ st Running r)
                                       , AX $ (sysOf $ activatingCalls r) <=== (st Running r \/ st Ready r)
                                       ])) ++
                                    ((\ b -> (st Running r) ===> AX b) <$> 
                                       [ ((st Ready r) ===> (othersc r))
                                       , ((st Ready r) ===> (otherhp r))
                                       , ((st Suspended r) ===> (sysOf $ terminatingCalls r))
                                       , ((st Waiting r) ===> (waitSc r))
                                       ] ++
                                       (if _isISR r then [] else [(st Ready r) <=== (otherCouldPreempt r)]) ++
                                       ((\ tc -> (st Suspended r) <=== (g_syscall `is` tc)) <$> terminatingCalls r) ++
                                       ((\ wc -> (st Waiting r) <=== wc) <$> wsc r)
                                       ) ++
                                    ((\ b -> (st Ready r) ===> b) <$>
                                       (((\ sh -> (allOtherPreemptable r) ===> AX (((g_syscall `is` sh) /\ (st Running r)) ===> (embed $ isHighestPrio r))) <$> schedulingCalls) ++
                                       (if _isISR r then [] else ((\ sh -> (allOtherPreemptable r) ===> AX ((g_syscall `is` sh /\ (embed $ isHighestPrio r)) ===> (st Running r))) <$> schedulingCalls)) ++
                                       [ AX (st Ready r \/ st Running r) ])) ++
                                     [(st Waiting r) ===>
                                       (fAll (evts r) (\ e -> (embed $ isWaitingFor e r) ===>
                                         (((allOtherPreemptable r ===> (AX $ (embed $ eventSet e) ===> ((st Running r) <==> (embed $ isHighestPrio r)))) 
                                         /\ ((neg $ allOtherPreemptable r) ===> (AX $ (embed $ eventSet e) ===> (st Ready r))))
                                         /\ AX ((st Waiting r /\ (embed $ neg $ eventSet e)) \/ st Running r \/ st Ready r))))]) =<< (Nel.toList $ runnables ?sc))
  where
    iconnective :: Prop t => Runnable -> t -> t -> t
    iconnective r = if _isISR r then (===>) else (<==>)
    ape :: Embeddable (NExpr NBool) t => Runnable -> [t]
    ape ru = (\ or -> embed $ neg $ isRunning or) <$> ((filter (not . canPreempt ru) $ ET <$> _tasks ?sc) \\ [ru])
    allOtherPreemptable :: Embeddable (NExpr NBool) t => Runnable -> t
    allOtherPreemptable ru = embed $ fAll ((filter (not . canPreempt ru) $ ET <$> _tasks ?sc) \\ [ru]) (\ or -> neg $ isRunning or)
    othersc :: Embeddable (NExpr NBool) t => Runnable -> t
    othersc ru = sysOf $ filter (not . isWaitCall) $ schedulingCalls \\ terminatingCalls ru
    otherhp :: Embeddable (NExpr NBool) t => Runnable -> t
    otherhp ru = embed $ exst ((filter (flip canPreempt $ ru) $ Nel.toList $ runnables ?sc) \\ [ru]) (isHighestPrio)
    otherCouldPreempt :: Runnable -> CTL
    otherCouldPreempt ru = othersc ru /\ otherhp ru
    sysOf :: Embeddable (NExpr NBool) t => [Syscall] -> t
    sysOf sys = embed $ InSetExpr (ref g_syscall) (SetExpr $ lit <$> nub sys)
    st :: Embeddable (NExpr NBool) t => TaskState -> Runnable -> t
    st stat ru = if stat `elem` (Nel.toList $ stateAt ru <$> states ?os) then embed $ rstate ru === lit stat else embed $ lit False
    evts :: Runnable -> [Event]
    evts (ET x) = _tevents x
    evts _ = []
    wsc :: Embeddable (NExpr NBool) t => Runnable -> [t]
    wsc ru = (\e -> embed $ ( neg $ eventSet e) /\ (isWaitingFor e ru)) <$> (evts ru)
    waitSc :: Embeddable (NExpr NBool) t => Runnable -> t
    waitSc ru = embed $ exst (evts ru) (\e -> (neg $ eventSet e) /\ (isWaitingFor e ru))

g_idleCanOnlyRunWhenNoneReady :: (?sc :: StaticConf) => (?os :: Runtime) => Spec
g_idleCanOnlyRunWhenNoneReady = CTLSpec (Just "idleCanOnlyRunWhenNoneReady") $ AG $ embed $ (isRunning $ ET $ idleTask 0) <==> fAll ((Nel.toList $ runnables ?sc) \\ [ET $ idleTask 0]) (\ r -> neg $ isReady r \/ isRunning r) 

g_resourceStateBehavesCorrectly :: (?sc :: StaticConf) => (?os :: Runtime) => [Spec]
g_resourceStateBehavesCorrectly = CTLSpec Nothing . AG <$>
  ((\ r ->
       if _internal r
       then
         [ occ r ===> fAll (ET <$> tasksHavingIntResource r) (\ t -> (embed $ isRunning t) ===> AX ((neg $ occ r) <==> embed ((isSuspended t \/ isWaiting t) /\ fAll ((ET <$> tasksHavingIntResource r) \\ [t]) (\ ot -> neg $ isRunning ot))))
         , (neg $ occ r) ===> AX (occ r <==> iresTaskStarted r)
         ]
       else
         [ (neg $ occ r) ===> AX (occ r <==> getCalled r)
         , occ r ===> AX ((neg $ occ r) <==> releaseCalled r)
         ]
   ) =<< _resources ?sc)
  where
    occ :: Embeddable (NExpr NBool) t => Resource -> t
    occ r = embed $  resOccupied r
    getCalled :: Embeddable (NExpr NBool) t => Resource -> t
    getCalled r = g_syscall `is` (GetResource $ _rname r) 
    releaseCalled :: Embeddable (NExpr NBool) t => Resource -> t
    releaseCalled r = g_syscall `is` (ReleaseResource $ _rname r)
    iresTaskStarted r = embed $ exst (ET <$> tasksHavingIntResource r) (\ t -> isRunning t)
  
mainModule :: (?sc :: StaticConf) => (?os :: Runtime) => Module (StaticConf, Runtime)
mainModule = addAll highestPrioDefs $ addAll taskEventStates $ addAll runnableAbbs $ addAll ceilingPrios $ addAll resourceStates $ addAll eventStates $ addAll runnableStates $ add g_systemState $ add g_runningState $ add g_syscall $ add g_interruptsState $ 
  Module "main" [] [] [] [] instances [] [] [] [] ([
    CTLSpec (Just "onlyOneRunning") $ AG $ embed (exactly 1 (Nel.toList $ runnables ?sc) $ isRunning),
    CTLSpec (Just "disableIRBeforEnable") $ AG $ (g_interruptsState `is` Enabled) ===> AX (neg $ g_syscall `is` EnableIR),
    CTLSpec (Just "isrPriosHigher") $ embed $ fAll (interrupts ?sc) $ \ i -> fAll (_tasks ?sc) $ \ t -> NGt (prio i) (prio $ ET t),
    g_interruptsStateIsRespected,
    g_interruptsStateTransitions,
    g_idleCanOnlyRunWhenNoneReady
  ]
  ++ g_cat1NoAPICalls
  ++ g_cat2CertainApiCalls
  ++ g_fastStateTransitionsAreValid
  ++ g_nonPreemptableTasksNotPreempted
  ++ g_resourceStateBehavesCorrectly
  ) [] (const "MAINMODULE") 
    where 
      runnableStates :: [StateVar NEnum]
      runnableStates = Nel.toList $ g_runnableState <$> (runnables ?sc)
      runnableAbbs :: [StateVar NEnum]
      runnableAbbs = Nel.toList $ g_runnableAbb <$> (runnables ?sc)
      ceilingPrios :: [StateVar NInt]
      ceilingPrios = Nel.toList $ g_ceilingPrio <$> (runnables ?sc)
      eventStates :: [StateVar NBool]
      eventStates = g_eventState <$> (_events ?sc)
      resourceStates :: [StateVar NBool]
      resourceStates = g_resourceState <$> (_resources ?sc)
      taskEventStates :: [ArrayVar NBool]
      taskEventStates = g_taskEventState <$> (_tasks ?sc)
      highestPrioDefs :: [StateVar NBool]
      highestPrioDefs = Nel.toList $ g_highestPrio <$> (runnables ?sc)
      instances :: [Variable NModule]
      instances = (inst taskModule <$> (_tasks ?sc)) ++ (inst isr1Module <$> (_isr1s ?sc)) ++ (inst isr2Module <$> (_isr2s ?sc)) ++ (inst resourceModule <$> (_resources ?sc)) ++ (inst eventModule <$> (_events ?sc))


instance Exportable (StaticConf, Runtime) where 
  exportNuSMV (sc, os) = let ?sc = sc
                             ?os = os
                         in
                           [ nshow $ mainModule
                           , nshow $ taskModule
                           , nshow $ isr1Module
                           , nshow $ isr2Module
                           , nshow $ resourceModule
                           , nshow $ eventModule
                           ]
  exportCOOLModel (_, os) = [show $ KModel $ ufold (\ (pr, n, l, su) m -> if null pr then insert (stateName StartOS n) (stateStruct StartOS l su) m else foldr (\ (sc, _) accu -> insertWith (flip const) (stateName sc n) (stateStruct sc l su) accu) m pr) mempty (_graph os)]
    where
      stateName :: Syscall -> Node -> State
      stateName s n = "s" <> show n <> "_" <> show s
      stateStruct :: Syscall -> OsekState -> Adj Syscall -> (Atoms, [State])
      stateStruct sc l su = ([show sc, _abb l, objName (_running l), show (_irqState l)], uncurry stateName <$> su)
