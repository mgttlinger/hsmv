{ mkDerivation, aeson, base, bytestring, containers, fgl, lens, lib
, MissingH, parallel, parsec, scientific, text
}:
mkDerivation {
  pname = "hsmv";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    aeson base bytestring containers fgl lens MissingH parallel parsec
    scientific text
  ];
  executableHaskellDepends = [ aeson base bytestring fgl lens ];
  testHaskellDepends = [ base ];
  homepage = "https://gitlab.com/mgttlinger/hsmv";
  license = lib.licenses.unfree;
  hydraPlatforms = lib.platforms.none;
  mainProgram = "hsmv-exe";
}
